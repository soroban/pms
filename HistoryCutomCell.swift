//
//  HistoryCutomCell.swift
//  pms
//
//  Created by soroban11 on 2015/03/26.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

class HistoryCutomCell: UITableViewCell {
    var activeLabel: UILabel!
    var colorLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // セル内セット
    func configureCell(contents:String, type:Int, atIndexPath indexPath: NSIndexPath){
        var subviews = self.subviews
        for subview in subviews {
            for sub in subview.subviews {
                if(sub.tag == 99999){
                    sub.removeFromSuperview()
                }
            }
        }
        var subviewsIOS8 = self.subviews
        for subview in subviewsIOS8 {
            if(subview.tag == 99999){
                subview.removeFromSuperview()
            }
        }
        let rect = UIScreen.mainScreen().applicationFrame
        let font = UIFont(name: "HelveticaNeue", size: 12)
        self.activeLabel = UILabel(frame: CGRectMake(30,0,rect.width-30,45))
        self.activeLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
        self.activeLabel.text = contents
        self.activeLabel.font = font
        self.activeLabel.tag = 99999
        self.addSubview(self.activeLabel)
        
        self.colorLabel = UILabel(frame: CGRectMake(5,16,13,13))
        if(type == 2){
            colorLabel.backgroundColor = UIColor(red:0.84, green:0.88, blue:0.95, alpha:1.0)
        }else{
            colorLabel.backgroundColor = UIColor(red:0.95, green:0.84, blue:0.88, alpha:1.0)
        }
        self.colorLabel.tag = 99999
        self.addSubview(self.colorLabel)
    }
}