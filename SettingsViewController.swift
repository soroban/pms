//
//  SettingsViewController.swift
//  pms
//
//  Created by soroban11 on 2015/03/30.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    @IBOutlet var myTable: UITableView!
    var myItems = [NSLocalizedString("id00046",comment: ""),NSLocalizedString("id00047",comment: "")]

    override func viewDidLoad() {
        super.viewDidLoad()
        let v:UIView = UIView(frame:CGRectMake(0, 0, app.screenWidth, 70));
        var frame:CGRect = CGRectMake(CGFloat(0),
            CGFloat(0),
            CGFloat(app.screenWidth),
            CGFloat(70))
        
        var topLabel = UILabel(frame: frame)
        topLabel.textAlignment = NSTextAlignment.Center
        topLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        topLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
        topLabel.text = NSLocalizedString("id00049",comment: "")
        v.addSubview(topLabel)
        
        var rect:CGRect = CGRectMake((app.screenWidth/2)-60, 25, 25, 25);
        let countryCode = NSLocale.preferredLanguages().first as! String
        if(countryCode == "ja"){
            var rect:CGRect = CGRectMake((app.screenWidth/2)-50, 25, 25, 25);
        }

        var image = UIImage(named: "airplane_takeoff.png")
        var imageView = UIImageView(image: image)
        imageView.frame = rect;
        v.addSubview(imageView)
        self.navigationItem.titleView = v


        var tableV:UIView = UIView(frame: CGRectZero)
        self.myTable.tableFooterView = tableV
        if self.myTable.respondsToSelector("layoutMargins") {
            self.myTable.layoutMargins = UIEdgeInsetsZero
        }
        self.myTable.separatorInset = UIEdgeInsetsZero

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        let font = UIFont(name: "HelveticaNeue", size: 17)
        cell.textLabel?.text = "\(myItems[indexPath.row])"
        cell.textLabel?.font = font
        if cell.respondsToSelector("layoutMargins") {
            cell.layoutMargins = UIEdgeInsetsZero;
        }
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath!) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if(indexPath.row == 0){
            // SubViewController へ遷移するために Segue を呼び出す
            performSegueWithIdentifier("goToAffirmation",sender: nil)
        }else if(indexPath.row == 1){
            performSegueWithIdentifier("goToIconDetal",sender: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}