//
//  AddAccHowmanyViewController.swift
//  pms
//
//  Created by soroban11 on 2015/01/16.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

protocol AddAccHowmanyViewControllerDelegate{
    func howmanyDidFinished(point: Int)
}

class AddAccHowmanyViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    var delegate: AddAccHowmanyViewControllerDelegate!
    // UIPickerView.
    var myUIPicker: UIPickerView = UIPickerView()
    var point:Int!
    var myValues: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        for ( var i = 1, n = app.getMaxPoint ; i <= n ; i++ ) {
            myValues.append(String(i))
        }

        // サイズを指定する.
        myUIPicker.frame = CGRectMake(0,self.view.bounds.height/10*3,self.view.bounds.width, 360.0)
        // Delegateを設定する.
        myUIPicker.delegate = self
        
        // DataSourceを設定する.
        myUIPicker.dataSource = self
        myUIPicker.selectRow(self.point-1, inComponent: 0, animated: true)
        // Viewに追加する.
        self.view.addSubview(myUIPicker)
        
        var unitLabel = UILabel(frame: CGRectMake(self.view.bounds.width-120,self.view.bounds.height/10*3+90,self.view.bounds.width/10*3, 33))
        unitLabel.textAlignment = NSTextAlignment.Center
        unitLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        unitLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
        unitLabel.text = NSLocalizedString("id00005",comment: "")
        self.view.addSubview(unitLabel)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //[戻る]が押される直前に呼ばれるメソッド
    override func viewWillDisappear(animated: Bool) {
        let viewControllers = self.navigationController?.viewControllers!
        if indexOfArray(viewControllers!, searchObject: self) == nil {
            if(self.point == 0){
                self.point = 1
            }
            self.delegate.howmanyDidFinished(self.point)
        }
        super.viewWillDisappear(animated)
    }
    
    func indexOfArray(array:[AnyObject], searchObject: AnyObject)-> Int? {
        for (index, value) in enumerate(array) {
            if value as! UIViewController == searchObject as! UIViewController {
                return index
            }
        }
        return nil
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    /*
    表示するデータ数を返す.
    */
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return myValues.count
    }
    
    /*
    値を代入する.
    */
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String {
        return myValues[row] as String
    }
    
    /*
    Pickerが選択された際に呼ばれる.
    */
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.point = myValues[row].toInt()
    }
}
