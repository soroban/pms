//
//  AddAccHowtoViewController.swift
//  pms
//
//  Created by soroban11 on 2015/01/14.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

protocol AddAccHowtoViewControllerDelegate{
    func howtoDidFinished(action: String)
}

class AddAccHowtoViewController: UIViewController {
    
    @IBOutlet var myTableView: UITableView!
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    var delegate: AddAccHowtoViewControllerDelegate!
    var action:String!
    var myItems = [""]
    var mySections = [NSLocalizedString("id00016",comment: "")]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.myTableView.registerClass(AffCustomCell.classForCoder(), forCellReuseIdentifier: "customCell")
        // Do any additional setup after loading the view.
    }

    //[戻る]が押される直前に呼ばれるメソッド
    override func viewWillDisappear(animated: Bool) {
        let viewControllers = self.navigationController?.viewControllers!
        if indexOfArray(viewControllers!, searchObject: self) == nil {
            let indexPath = NSIndexPath(forRow:0, inSection:0)
            var cell = self.myTableView.cellForRowAtIndexPath(indexPath) as! AffCustomCell
            self.delegate.howtoDidFinished(cell.myTextField.text)
        }
        super.viewWillDisappear(animated)
    }
    
    func indexOfArray(array:[AnyObject], searchObject: AnyObject)-> Int? {
        for (index, value) in enumerate(array) {
            if value as! UIViewController == searchObject as! UIViewController {
                return index
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if indexPath.section == 0 {
            var cell: AffCustomCell = self.myTableView.dequeueReusableCellWithIdentifier("customCell") as! AffCustomCell
            var exit = true
            if(self.action.isEmpty){
                exit = false
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.Blue
            cell.configureCell(exit, aff: self.action, row: 44.0, atIndexPath : indexPath)
            return cell
        }
        var cell = tableView.dequeueReusableCellWithIdentifier("customCell", forIndexPath: indexPath) as! UITableViewCell
        return cell
    }
    
    /*
    セクションの数を返す.
    */
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return mySections.count
    }
    
    /*
    セクションのタイトルを返す.
    */
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return mySections[section]
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if (view.isKindOfClass(UITableViewHeaderFooterView)) {
            var tableViewHeaderFooterView:UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
            tableViewHeaderFooterView.textLabel.text = mySections[section]
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
