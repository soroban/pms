//
//  AddPrizeViewController.swift
//  pms
//
//  Created by soroban11 on 2015/02/12.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit
import CoreData
import AssetsLibrary

class AddPrizeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AddPrizeHowtoViewControllerDelegate, AddPrizeHowmanyViewControllerDelegate, UIActionSheetDelegate {
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    //カメラ or ライブラリーから選択された画像データ
    var selectImage: UIImage!
    // テーブルを用意
    var myTable: UITableView!
    
    // Tableで使用する配列を定義する.
    var myItems: NSArray = ["", ""]
    
    var selectId: Int!
    var howTo: String = ""
    var howMany: Int = 0
    var path: String!
    var CellHeight: CGFloat!
    
    let TAG_IMAGE_VIEW = 99999
    var howToStr: String =  NSLocalizedString("id00003",comment: "")
    var howManyStr: String = NSLocalizedString("id00004",comment: "")
    var noImageFile: String = "l_e_others_500.png"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var contoroller = self.navigationController
        var addPrizeNavigationController: AddPrizeNavigationController = contoroller as! AddPrizeNavigationController
        self.selectId = addPrizeNavigationController.selectId
        // UIImageViewを作成する.        
        //編集
        self.path = NSBundle.mainBundle().pathForResource(noImageFile, ofType: nil)
        if(self.selectId != nil){
            var prizeDetail:Prize = self.getPrizeFromId(self.selectId)
            if(!prizeDetail.imagePath.isEmpty){
                self.path = String(format:"%@/Documents/%@",NSHomeDirectory(), prizeDetail.imagePath)
            }
            self.howTo = prizeDetail.gift
            self.howMany = Int(prizeDetail.point)
            self.myItems = [self.howTo, String(format: "%d" + NSLocalizedString("id00005",comment: ""), self.howMany)]
        }
            //新規作成
        else{
            self.myItems = [self.howToStr, self.howManyStr]
            // 表示する画像を設定する.
        }
        
        var anError : NSError?
        var sceneData = NSData(contentsOfFile: self.path, options: NSDataReadingOptions.DataReadingUncached, error: &anError)
        var tmpImage = UIImage(data: sceneData!)
        var scale = self.app.screenWidth/tmpImage!.size.width
        self.CellHeight = tmpImage!.size.height * scale
        
        self.selectImage = tmpImage

        // テーブルを用意して、表示
        self.myTable = UITableView(frame: CGRectMake(0, 0, self.app.screenWidth, self.app.screenHeight), style: .Grouped)
        self.myTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "data")
        self.myTable.registerClass(ImageViewCell.self, forCellReuseIdentifier: "imageCell")
        self.myTable.dataSource = self
        self.myTable.delegate = self
//        self.myTable.scrollEnabled = false
        self.view.addSubview(self.myTable)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    Cellが選択された際に呼び出される.
    */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if(indexPath.section == 1){
            if(indexPath.row == 0){
                //遷移するために Segue を呼び出す
                performSegueWithIdentifier("goToHowto",sender: nil)
            }
            if(indexPath.row == 1){
                //遷移するために Segue を呼び出す
                performSegueWithIdentifier("goToHowmany",sender: nil)
            }
        }else{
            if objc_getClass("UIAlertController") != nil {
                var alertController = UIAlertController(title: "", message: NSLocalizedString("id00010",comment: ""), preferredStyle: .ActionSheet)
                
                let cameraAction = UIAlertAction(title: NSLocalizedString("id00007",comment: ""), style: .Default) {
                    action in self.pickImageFromCamera()
                }
                let libraryAction = UIAlertAction(title: NSLocalizedString("id00008",comment: ""), style: .Default) {
                    action in self.pickImageFromLibrary()
                }
                let cancelAction = UIAlertAction(title: NSLocalizedString("id00009",comment: ""), style: .Cancel, handler: nil)
                
                alertController.addAction(cameraAction)
                alertController.addAction(libraryAction)
                alertController.addAction(cancelAction)
                
                presentViewController(alertController, animated: true, completion: nil)
            }else{
                var sheet: UIActionSheet = UIActionSheet();
                sheet.title  = NSLocalizedString("id00005",comment: "");
                sheet.delegate = self;
                sheet.addButtonWithTitle(NSLocalizedString("id00007",comment: ""));
                sheet.addButtonWithTitle(NSLocalizedString("id00008",comment: ""));
                sheet.addButtonWithTitle(NSLocalizedString("id00009",comment: ""));
                sheet.cancelButtonIndex = 2;
                sheet.showInView(self.view);
            }
        }
    }
    
    /*
     *テーブルに表示する配列の総数を返す.
     */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1){
            return myItems.count
        }
        return 1
    }

    /*
    セクションの数を返す.
    */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return self.CellHeight
        }
        return 45
    }


    // セルの値を設定
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "data")
        if(indexPath.section == 0){
            var cellImage: ImageViewCell = self.myTable.dequeueReusableCellWithIdentifier("imageCell") as! ImageViewCell
            cellImage.configureCell(atIndexPath: indexPath)
            //タッチを可能にする
            cellImage.myImageView.userInteractionEnabled = true;
            // 画像をUIImageViewに設定する.
            cellImage.myImageView.image = self.selectImage
            cellImage.myImageView.frame = CGRectMake(0, 0, self.app.screenWidth, self.CellHeight)
            cellImage.myImageView.contentMode = UIViewContentMode.ScaleAspectFill
            cellImage.addSub()
            return cellImage
        }else{
            let font = UIFont(name: "HelveticaNeue", size: 15)
            cell.textLabel?.text = "\(myItems[indexPath.row])"
            cell.textLabel?.font = font
            cell.textLabel?.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            cell.selectionStyle = UITableViewCellSelectionStyle.Blue
            tableView.separatorStyle = UITableViewCellSeparatorStyle.None
            cell.detailTextLabel?.text=NSLocalizedString("id00006",comment: "")
        }
        
        return cell
    }
    
    //遷移直前で呼ばれる
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "goToHowto") {
            let addPrizeHowtoViewController: AddPrizeHowtoViewController = segue.destinationViewController as! AddPrizeHowtoViewController
            addPrizeHowtoViewController.delegate = self
            addPrizeHowtoViewController.action = self.howTo
        }
        if (segue.identifier == "goToHowmany") {
            let addPrizeHowmanyViewController: AddPrizeHowmanyViewController = segue.destinationViewController as! AddPrizeHowmanyViewController
            addPrizeHowmanyViewController.delegate = self
            addPrizeHowmanyViewController.point = self.howMany
        }
    }
    
    //キャンセルボタンが押されたとき
    @IBAction func closeMe(sender: AnyObject) {
        return dismissViewControllerAnimated(true, completion: nil)
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return  50
    }
    
    //[何をしてポイントをためるか？]の入力画面から戻ってきたとき
    func howtoDidFinished(actionText: String){
        var howTo: String = self.howToStr
        var howMany: String = self.howManyStr
        
        let deleteTarget = NSCharacterSet.whitespaceCharacterSet
        var receiveActionText = actionText.stringByTrimmingCharactersInSet(deleteTarget())
        self.howTo = receiveActionText
        
        if(!receiveActionText.isEmpty) {
            howTo = receiveActionText
        }
        if(self.howMany > 0 && self.howMany <= app.useMaxPoint) {
            howMany = String(format: "%d" + NSLocalizedString("id00005",comment: ""), self.howMany)
        }
        
        self.myItems = [howTo, howMany]
        self.myTable.reloadData()
    }
    
    //[その時の獲得ポイント？]の入力画面から戻ってきたとき
    func howmanyDidFinished(getPoint: Int){
        var howTo: String = self.howToStr
        var howMany: String = self.howManyStr
        
        self.howMany = getPoint
        if(!self.howTo.isEmpty){
            howTo = self.howTo
        }
        if(getPoint <= 0 || getPoint > app.useMaxPoint) {
            howMany = self.howManyStr
        }else{
            howMany = String(format: "%d"+NSLocalizedString("id00005",comment: ""), getPoint)
        }
        self.myItems = [howTo, howMany]
        self.myTable.reloadData()
    }
    
    
    //貯めるへ新規追加するためにPrizeのIDの最大値を取得
    func getPrizeMaxId() -> (Int){
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Prize")
        var obj : NSManagedObject? = nil
        
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.fetchLimit = 1
        //ORDER BY を指定
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = [sortDescriptor]
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Prize]!
        var returnId:Int = 0
        for result in results {
            returnId = Int(result.id)
        }
        return returnId
    }
    
    //データを新規追加
    func addPrize(id : Int ) {
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let entity: NSEntityDescription! = NSEntityDescription.entityForName("Prize", inManagedObjectContext: managedContext)
        var newData = Prize(entity: entity, insertIntoManagedObjectContext: managedContext)
        newData.id = id
        newData.disp = true
        newData.gift = self.howTo
        newData.point = self.howMany
        newData.createdAt =  NSDate()
        newData.order = id
        if((self.selectImage) != nil){
            newData.imagePath = String(format:"prize_%d.jpg", id)
        }
        else{
            newData.imagePath = ""
        }
        newData.lastUsedAt =  NSDate()
        newData.num = 0
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error!.userInfo)")
        }
    }
    
    //データを編集
    func editPrize(id : Int ) {
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Prize")
        var obj : NSManagedObject? = nil
        
        fetchRequest.returnsObjectsAsFaults = false
        //WHERE を指定
        fetchRequest.predicate =  NSPredicate(format: "id = %d", id)
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Prize]!
        for result in results {
            result.id = id
            result.disp = true
            result.gift = self.howTo
            result.point = self.howMany
            result.createdAt =  NSDate()
            if((self.selectImage) != nil){
                result.imagePath = String(format:"prize_%d.jpg", id)
            }
            result.lastUsedAt =  NSDate()
            result.num = 0
        }
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not update \(error), \(error!.userInfo)")
        }
        
    }
    
    //画像を保存
    func saveImage(id : Int ) {
        if((self.selectImage) != nil){
            //通常画
            var bimage:UIImage = self.cropThumbnailImage(self.selectImage, w: 500, h: 380)
            var data:NSData = UIImageJPEGRepresentation(bimage, 0.8)
            var path:String = String(format:"%@/Documents/prize_%d.jpg",NSHomeDirectory(), id)
            data.writeToFile(path, atomically: true)
            //サムネイル画像
            var thumnail:UIImage = self.cropThumbnailImage(self.selectImage, w: 300, h: 300)
            var dataThumb:NSData = UIImageJPEGRepresentation(thumnail, 0.8)
            var pathThumb:String = String(format:"%@/Documents/thumb_prize_%d.jpg",NSHomeDirectory(), id)
            dataThumb.writeToFile(pathThumb, atomically: true)
        }
    }
    
    //タッチイベントを取得して実行するメソッド
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesEnded(touches, withEvent: event)
        for touch: AnyObject in touches {
            var t: UITouch = touch as! UITouch
            if t.view.tag == self.TAG_IMAGE_VIEW{
                if objc_getClass("UIAlertController") != nil {
                    var alertController = UIAlertController(title: "", message: NSLocalizedString("id00010",comment: ""), preferredStyle: .ActionSheet)
                
                    let cameraAction = UIAlertAction(title: NSLocalizedString("id00007",comment: ""), style: .Default) {
                    action in self.pickImageFromCamera()
                    }
                    let libraryAction = UIAlertAction(title: NSLocalizedString("id00008",comment: ""), style: .Default) {
                        action in self.pickImageFromLibrary()
                    }
                    let cancelAction = UIAlertAction(title: NSLocalizedString("id00009",comment: ""), style: .Cancel, handler: nil)
                
                    alertController.addAction(cameraAction)
                    alertController.addAction(libraryAction)
                    alertController.addAction(cancelAction)
                
                    presentViewController(alertController, animated: true, completion: nil)
                }else{
                    var sheet: UIActionSheet = UIActionSheet();
                    sheet.title  = NSLocalizedString("id00005",comment: "");
                    sheet.delegate = self;
                    sheet.addButtonWithTitle(NSLocalizedString("id00007",comment: ""));
                    sheet.addButtonWithTitle(NSLocalizedString("id00008",comment: ""));
                    sheet.addButtonWithTitle(NSLocalizedString("id00009",comment: ""));
                    sheet.cancelButtonIndex = 2;
                    sheet.showInView(self.view);
                }
            }
        }
    }
    
    func actionSheet(sheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        switch (buttonIndex) {
        case 0:
            self.pickImageFromCamera()
            break;
        case 1:
            self.pickImageFromLibrary()
            break;
        default:
            break
        }
    }
    
    // 写真を撮ってそれを選択
    func pickImageFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let controller = UIImagePickerController()
            controller.delegate = self
            controller.sourceType = UIImagePickerControllerSourceType.Camera
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    // ライブラリから写真を選択する
    func pickImageFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            let controller = UIImagePickerController()
            controller.delegate = self
            controller.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(controller, animated: true, completion: nil)
        }else{
            let controller = UIImagePickerController()
            controller.delegate = self
            controller.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
            self.presentViewController(controller, animated: true, completion: nil)
        }
        
    }
    
    
    // 写真を選択した時に呼ばれる
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        if info[UIImagePickerControllerOriginalImage] != nil {
            self.selectImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            var indexPath = NSIndexPath(forRow:0, inSection:0)
            var cell = self.myTable.cellForRowAtIndexPath(indexPath) as! ImageViewCell
            var bimage:UIImage = self.cropThumbnailImage(self.selectImage, w: 500, h: 380)
            self.selectImage = bimage
            cell.myImageView.image = self.selectImage
        }else if info[UIImagePickerControllerReferenceURL] != nil {
            let library = ALAssetsLibrary()
            let imageUrl: NSURL? = info[UIImagePickerControllerReferenceURL] as? NSURL
            library.assetForURL(imageUrl, resultBlock: {
                (asset: ALAsset!) in
                if asset != nil {
                    var assetRep: ALAssetRepresentation = asset.defaultRepresentation()
                    var iref = assetRep.fullResolutionImage().takeUnretainedValue()
                    self.selectImage =  UIImage(CGImage: iref)

                    var indexPath = NSIndexPath(forRow:0, inSection:0)
                    var cell = self.myTable.cellForRowAtIndexPath(indexPath) as! ImageViewCell
                    var bimage:UIImage = self.cropThumbnailImage(self.selectImage, w: 500, h: 380)
                    self.selectImage = bimage
                    cell.myImageView.image = self.selectImage
                }
                }, failureBlock: {
                    (error: NSError!) in
                    
                    NSLog("Error!")
                }
            )
        }
        
    }
    
    //保存「使う」を保存
    @IBAction func prizeAction(){
        var errorMes:String = ""
        var erroFlg:Bool = false
        //入力確認
        if(self.howTo.isEmpty){
            errorMes = "・" + NSLocalizedString("id00003",comment: "")
            erroFlg = true
        }
        if(self.howMany <= 0 || self.howMany > app.useMaxPoint) {
            if(!errorMes.isEmpty){
                errorMes = errorMes + "\n"
            }
            errorMes = errorMes + "・" + NSLocalizedString("id00004",comment: "")
            erroFlg = true
        }
        
        //エラーメッセージを表示
        if(erroFlg){
            var alertController:UIAlertController = UIAlertController(title: NSLocalizedString("id00011",comment: ""), message: errorMes, preferredStyle: UIAlertControllerStyle.Alert)
            let otherAction = UIAlertAction(title: "OK", style: .Default, handler:nil)
            alertController.addAction(otherAction)
            presentViewController(alertController, animated: true, completion: nil)
        }
            //データを保存
        else{
            //編集
            if(self.selectId != nil){
                //　coredata　へ保存
                self.editPrize(self.selectId)
                
                //画像を保存
                self.saveImage(self.selectId)
            }//新規
            else{
                //idの最大値を取得
                var id = self.getPrizeMaxId()
                //　coredata　へ保存
                self.addPrize(id+1)
                //画像を保存
                self.saveImage(id+1)
            }
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func cropThumbnailImage(image :UIImage, w:Int, h:Int) ->UIImage
    {
        // リサイズ処理
        
        let origRef    = image.CGImage;
        var origWidth  = Int(CGImageGetWidth(origRef))
        var origHeight = Int(CGImageGetHeight(origRef))
        var resizeWidth:Int = 0, resizeHeight:Int = 0
        
        // リサイズ処理
        if (image.imageOrientation==UIImageOrientation.Up ||
            image.imageOrientation==UIImageOrientation.Down) {
                // 横位置
                origWidth  = Int(CGImageGetWidth(origRef))
                origHeight = Int(CGImageGetHeight(origRef))
        } else {
            // 縦位置
            origWidth  = Int(CGImageGetHeight(origRef))
            origHeight = Int(CGImageGetWidth(origRef))
        }
        
        if (origWidth < origHeight) {
            resizeWidth = w
            resizeHeight = origHeight * resizeWidth / origWidth
        } else {
            resizeHeight = h
            resizeWidth = origWidth * resizeHeight / origHeight
        }
        
        let resizeSize = CGSizeMake(CGFloat(resizeWidth), CGFloat(resizeHeight))
        UIGraphicsBeginImageContext(resizeSize)
        
        image.drawInRect(CGRectMake(0, 0, CGFloat(resizeWidth), CGFloat(resizeHeight)))
        
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // 切り抜き処理
        
        let cropRect  = CGRectMake(
            CGFloat((resizeWidth - w) / 2),
            CGFloat((resizeHeight - h) / 2),
            CGFloat(w), CGFloat(h))
        let cropRef   = CGImageCreateWithImageInRect(resizeImage.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef)
        
        return cropImage!
    }
    
    func getPrizeFromId(id: Int)->Prize{
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Prize")
        var obj : NSManagedObject? = nil
        
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.fetchLimit = 1
        //ORDER BY を指定
        fetchRequest.predicate =  NSPredicate(format: "id = %d", id)
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Prize]!
        var prize:Prize!
        for result in results {
            prize = result
        }
        return prize
    }
}