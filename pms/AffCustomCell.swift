//
//  AffCustomCellViewCell.swift
//  pms
//
//  Created by soroban11 on 2015/04/03.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit
import CoreData

class AffCustomCell: UITableViewCell {
    var myTextField:UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(exist:Bool, aff:String, row:CGFloat, atIndexPath indexPath: NSIndexPath){
        //目標の設定済みか？
        let rect = UIScreen.mainScreen().applicationFrame
        self.myTextField = UITextField(frame: CGRectMake(0, 0, rect.width, row))
        if(exist){
            self.myTextField.text = aff
        }else{
            self.myTextField.text = ""
            self.myTextField.placeholder = NSLocalizedString("id00051",comment: "")
        }
        self.myTextField.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
        self.myTextField.borderStyle = UITextBorderStyle.RoundedRect
    
        self.addSubview(self.myTextField)
    }
//    func textFieldShouldReturn(textField: UITextField!) -> Bool {
//        self.endEditing(true)
//        return
//    }
}
