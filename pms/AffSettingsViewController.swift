//
//  AffSettingsViewController.swift
//  pms
//
//  Created by 大口 尚紀(管理者) on 2015/04/03.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

class AffSettingsViewController: UIViewController {
    @IBOutlet var myTable: UITableView!
    
    
    var myItems = ["目標設定(アファーメーション)の設定", "表示"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var v:UIView = UIView(frame: CGRectZero)
        self.myTable.tableFooterView = v
        //self.myTable.layoutMargins = UIEdgeInsetsZero
        self.myTable.separatorInset = UIEdgeInsetsZero


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        let font = UIFont(name: "HelveticaNeue", size: 15)
        cell.textLabel?.text = "\(myItems[indexPath.row])"
        cell.textLabel?.font = font
        //cell.layoutMargins = UIEdgeInsetsZero;
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
