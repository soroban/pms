//
//  AffSwCustomCell.swift
//  pms
//
//  Created by soroban11 on 2015/04/07.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

class AffSwCustomCell: UITableViewCell {
    var mySwitch:UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(atIndexPath indexPath: NSIndexPath){
        self.mySwitch = UISwitch()
        let rect = UIScreen.mainScreen().applicationFrame
        self.mySwitch.layer.position = CGPoint(x: rect.width*2/3, y: 22)
        self.addSubview(self.mySwitch)
    }
}
