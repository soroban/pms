//
//  Affirmation.h
//  pms
//
//  Created by soroban on 2014/12/05.
//  Copyright (c) 2014年 soroban. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Affirmation : NSManagedObject

@property (nonatomic, retain) NSString * aim;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDate * distpAt;

@end
