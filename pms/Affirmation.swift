//
//  pms.swift
//  pms
//
//  Created by soroban11 on 2015/02/17.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import Foundation
import CoreData

class Affirmation: NSManagedObject {

    @NSManaged var aim: String
    @NSManaged var createdAt: NSDate
    @NSManaged var dispFlg: Bool

}
