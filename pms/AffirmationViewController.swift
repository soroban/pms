//
//  AffirmationViewController.swift
//  pms
//
//  Created by soroban11 on 2015/04/02.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit
import CoreData

class AffirmationViewController: UIViewController,UITextFieldDelegate {
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    var mySections = [NSLocalizedString("id00052",comment: ""),"","",NSLocalizedString("id00053",comment: "")]
    var myValues = NSMutableArray()
    
    @IBOutlet var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAff()
        self.myTableView.registerClass(AffCustomCell.classForCoder(), forCellReuseIdentifier: "customCell")
        self.myTableView.registerClass(AffSwCustomCell.classForCoder(), forCellReuseIdentifier: "customAffCell")
        self.myTableView.allowsSelection = false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func exitAff() -> Bool{
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Affirmation")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        var res: Affirmation!
        //ORDER BY を指定
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Affirmation]!
        if(results.count > 0){
            return true
        }else{
            return false
        }
    }

    func getAff(){
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Affirmation")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        //ORDER BY を指定
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Affirmation]!
        for result in results {
            self.myValues.addObject(result)
        }
    }
    
    func removeAff(){
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Affirmation")
        var obj : NSManagedObject? = nil
        
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Affirmation]!
        var bas: NSManagedObject!
        for bas in results
        {
            managedContext.deleteObject(bas as NSManagedObject)
        }
        managedContext.save(nil)
    }
    
    
    //データを新規追加
    func addAff(aim : String, sw:Bool ) {
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let entity: NSEntityDescription! = NSEntityDescription.entityForName("Affirmation", inManagedObjectContext: managedContext)
        var newData = Affirmation(entity: entity, insertIntoManagedObjectContext: managedContext)
        newData.aim = aim
        newData.createdAt =  NSDate()
        if(sw){
            newData.dispFlg = true
        }else{
            newData.dispFlg = false
        }
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error!.userInfo)")
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        let viewControllers = self.navigationController?.viewControllers!
        if indexOfArray(viewControllers!, searchObject: self) == nil {
            self.removeAff()
            
            let indexPathSw = NSIndexPath(forRow:0, inSection:3)
            var cellSW = self.myTableView.cellForRowAtIndexPath(indexPathSw) as! AffSwCustomCell

            for ( var i = 0, n = 3 ; i < n ; i++ ) {
                let indexPath = NSIndexPath(forRow:0, inSection:i)
                var cell = self.myTableView.cellForRowAtIndexPath(indexPath) as! AffCustomCell

                // 戻るボタンが押された処理
                var affStr = cell.myTextField.text
                if(!cell.myTextField.text.isEmpty){
                    if(cellSW.mySwitch.on){
                        self.addAff(cell.myTextField.text, sw: true)
                    }else{
                        self.addAff(cell.myTextField.text, sw: false)
                    }
                }
            }
        }
        super.viewWillDisappear(animated)
    }
    
    func indexOfArray(array:[AnyObject], searchObject: AnyObject)-> Int? {
        for (index, value) in enumerate(array) {
            if value as! UIViewController == searchObject as! UIViewController {
                return index
            }
        }
        return nil
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let font = UIFont(name: "HelveticaNeue", size: 17)
        var cell: AffCustomCell = self.myTableView.dequeueReusableCellWithIdentifier("customCell") as! AffCustomCell
        var affStr:String = ""
        var exit = false
        if(self.myValues.count > indexPath.section){
            var aff:Affirmation = myValues[indexPath.section] as! Affirmation
            affStr = aff.aim
            exit = true
        }
        if indexPath.section != 3 {
            cell.selectionStyle = UITableViewCellSelectionStyle.Blue
            cell.configureCell(exit, aff: affStr, row: 44.0, atIndexPath : indexPath)
            cell.myTextField.delegate = self
            cell.myTextField.returnKeyType = UIReturnKeyType.Done
            return cell
        } else  {
            var affSw:Bool = false
            var exit = self.exitAff()
            if(exit){
                var aff:Affirmation = myValues[0] as! Affirmation
                affStr = aff.aim
                if(aff.dispFlg){
                    affSw = true
                }
            }

            var cell: AffSwCustomCell = self.myTableView.dequeueReusableCellWithIdentifier("customAffCell") as! AffSwCustomCell
            cell.selectionStyle = UITableViewCellSelectionStyle.Blue
            cell.configureCell(atIndexPath : indexPath)
            cell.textLabel?.text = NSLocalizedString("id00054",comment: "")
            if(affSw){
                cell.mySwitch.on = true
            }
            return cell
        }
    }

    /*
    セクションの数を返す.
    */
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return mySections.count
    }
    
    /*
    セクションのタイトルを返す.
    */
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return mySections[section]
    }
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if (view.isKindOfClass(UITableViewHeaderFooterView)) {
            var tableViewHeaderFooterView:UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
            tableViewHeaderFooterView.textLabel.text = mySections[section]
        }
    }


    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 10
        }
        else if(section != 3){
            return 0
        }
        return 40.0
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
