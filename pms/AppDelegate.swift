//
//  AppDelegate.swift
//  pms
//
//  Created by soroban on 2014/12/05.
//  Copyright (c) 2014年 soroban. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let maxMile: Int = 50000
    
    let getMaxPoint: Int = 50
    
    let useMaxPoint: Int = 10000
    
    //初期データ
    var saves=["読書をする(10分間)","食事を腹八分目ですます","早起きをする","片付けをする","ネタを書く(10分間)","運動をする(10分間)","心から人に「ありがとう」と言う"]
    var savesPoint=[5,5,10,10,5,5,10]

    var saves_en=["Read books(10 minutes)","Eat moderately","Get up early","Tidying up","Study(10 miuntes)","Take exercise(10 minutes)"]
    var savesPoint_en=[5,5,5,10,5,5]
    
    var prizes=["一日中ダラダラする","旅行にでかける","甘いものを食べる","カツカレーを食べる"]
    var prizesPoint=[4000,10000,500,3000]

    var prizes_en=["Laze around all day","Travel","Eat sweets","Eat favourite food"]
    var prizesPoint_en=[4000,10000,500,3000]

    //スクリーンの幅
    let screenWidth = UIScreen.mainScreen().bounds.size.width
    //スクリーンの高さ
    let screenHeight = UIScreen.mainScreen().bounds.size.height

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        var dic = ["firstLaunch": true]
        defaults.registerDefaults(dic)
        
        if defaults.boolForKey("firstLaunch") {
            // Some Process will be here
            self.updatePoint()
            
            var i=0
            let countryCode = NSLocale.preferredLanguages().first as! String
            if(countryCode == "ja"){
                for save in saves{
                    self.saveImage(i, type: "action")
                    self.addSave(i,howto: save,howmany: savesPoint[i])
                    i++;
                }
            }else{
                for save_en in saves_en{
                    self.saveImage(i, type: "action")
                    self.addSave(i,howto: save_en,howmany: savesPoint_en[i])
                    i++;
                }
            }
            
            i=0
            if(countryCode == "ja"){
                for prize in prizes{
                    self.saveImage(i, type: "prize")
                    self.addPrize(i,howto: prize,howmany: prizesPoint[i])
                    i++;
                }
            }
            else{
                for prize_en in prizes_en{
                    self.saveImage(i, type: "prize")
                    self.addPrize(i,howto: prize_en,howmany: prizesPoint_en[i])
                    i++;
                }
            }
            
            // off the flag to know if it is first time to launch
            defaults.setBool(false, forKey: "firstLaunch")
        }
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "soroban.com.pms" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as! NSURL
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("pms", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("pms.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            let dict = NSMutableDictionary()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict as [NSObject : AnyObject])
            
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog("Unresolved error \(error), \(error!.userInfo)")
                abort()
            }
        }
    }

    func updatePoint(){
        let managedContext: NSManagedObjectContext = self.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Point")
        var obj : NSManagedObject? = nil
        
        let entity: NSEntityDescription! = NSEntityDescription.entityForName("Point",inManagedObjectContext: managedContext)
        var newData = Point(entity: entity, insertIntoManagedObjectContext: managedContext)
        newData.point = 1192
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not update \(error), \(error!.userInfo)")
        }
    }
    
    //データを新規追加
    func addSave(id : Int, howto : String, howmany : Int) {
        let managedContext: NSManagedObjectContext = self.managedObjectContext!
        let entity: NSEntityDescription! = NSEntityDescription.entityForName("Save", inManagedObjectContext: managedContext)
        var newData = Save(entity: entity, insertIntoManagedObjectContext: managedContext)
        newData.id = id
        newData.disp = true
        newData.action = howto
        newData.point = howmany
        newData.createdAt =  NSDate()
        newData.order = id
        newData.imagePath = String(format:"action_%d.jpg", id)
        newData.lastUsedAt =  NSDate()
        newData.num = 0
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error!.userInfo)")
        }
    }
    
    //データを新規追加
    func addPrize(id : Int, howto : String, howmany : Int) {
        let managedContext: NSManagedObjectContext = self.managedObjectContext!
        let entity: NSEntityDescription! = NSEntityDescription.entityForName("Prize", inManagedObjectContext: managedContext)
        var newData = Prize(entity: entity, insertIntoManagedObjectContext: managedContext)
        newData.id = id
        newData.disp = true
        newData.gift = howto
        newData.point = howmany
        newData.createdAt =  NSDate()
        newData.order = id
        newData.imagePath = String(format:"prize_%d.jpg", id)
        newData.lastUsedAt =  NSDate()
        newData.num = 0
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error!.userInfo)")
        }
    }

    func saveImage(id : Int, type: String ) {
        var file:String = String(format:"org_%@_%d.png",type, id)
        var image = UIImage(named: file)
        //通常画
        var bimage:UIImage = self.cropThumbnailImage(image!, w: 500, h: 380)
        var data:NSData = UIImageJPEGRepresentation(bimage, 0.8)
        var path:String = String(format:"%@/Documents/%@_%d.jpg",NSHomeDirectory(),type, id)
        data.writeToFile(path, atomically: true)
        //サムネイル画像
        var thumnail:UIImage = self.cropThumbnailImage(image!, w: 300, h: 300)
        var dataThumb:NSData = UIImageJPEGRepresentation(thumnail, 0.8)
        var pathThumb:String = String(format:"%@/Documents/thumb_%@_%d.jpg",NSHomeDirectory(),type, id)
        dataThumb.writeToFile(pathThumb, atomically: true)
    }
    
    func cropThumbnailImage(image :UIImage, w:Int, h:Int) ->UIImage
    {
        // リサイズ処理
        let origRef    = image.CGImage;
        let origWidth  = Int(CGImageGetWidth(origRef))
        let origHeight = Int(CGImageGetHeight(origRef))
        var resizeWidth:Int = 0, resizeHeight:Int = 0
        
        if (origWidth < origHeight) {
            resizeWidth = w
            resizeHeight = origHeight * resizeWidth / origWidth
        } else {
            resizeHeight = h
            resizeWidth = origWidth * resizeHeight / origHeight
        }
        
        let resizeSize = CGSizeMake(CGFloat(resizeWidth), CGFloat(resizeHeight))
        UIGraphicsBeginImageContext(resizeSize)
        
        image.drawInRect(CGRectMake(0, 0, CGFloat(resizeWidth), CGFloat(resizeHeight)))
        
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // 切り抜き処理
        
        let cropRect  = CGRectMake(
            CGFloat((resizeWidth - w) / 2),
            CGFloat((resizeHeight - h) / 2),
            CGFloat(w), CGFloat(h))
        let cropRef   = CGImageCreateWithImageInRect(resizeImage.CGImage, cropRect)
        let cropImage = UIImage(CGImage: cropRef)
        
        return cropImage!
    }
}

