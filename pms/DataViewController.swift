//
//  DataViewController.swift
//  pms
//
//  Created by soroban11 on 2015/02/19.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit
import CoreData

class DataViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    var dateObject: AnyObject?
    var navBarHeight:CGFloat!
    var tabBarHeight:CGFloat!
    @IBOutlet var textLabel: UILabel!
    var currentYear:Int = 0
    var currentMonth:Int = 0
    var currentDay:Int = 0
    var prevMonthView:MonthView!
    var currentMonthView:MonthView!
    var nextMonthView:MonthView!
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    var dateLabel: UILabel!
    var flag:Bool = true
    // Tableで使用する配列を設定する
    var myTableView: UITableView!
    var myItems: [String] = []
    var Types: [Int] = []
    var section: String!
    
    override func viewDidLoad() {
        self.navigationController
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let statusBarHeight: CGFloat = UIApplication.sharedApplication().statusBarFrame.height
        var dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        var dateString:String = dateFormatter.stringFromDate(NSDate())
        
        if let obj: AnyObject = self.dateObject {
            dateString = obj.description
        }
        
        var dates:[String] = dateString.componentsSeparatedByString("/")
        currentYear  = dates[0].toInt()!
        currentMonth = dates[1].toInt()!
        currentDay = dates[2].toInt()!
        
        currentMonthView = MonthView(frame: CGRectMake(0, self.navBarHeight, UIScreen.mainScreen().bounds.size.width,UIScreen.mainScreen().bounds.size.height-self.navBarHeight-self.tabBarHeight),
            year:currentYear,month:currentMonth)
        var subviews = self.view.subviews
        for subview in subviews {
            subview.removeFromSuperview()
        }

        self.section = String(currentYear) + "-" + String(currentMonth) +  "-" + String(currentDay)
        // TableViewの生成する(status barの高さ分ずらして表示).
        let rect = UIScreen.mainScreen().applicationFrame
        myTableView = UITableView(frame: CGRectMake(0, rect.height, rect.width, rect.height / 3))
        
        // DataSourceの設定をする.
        myTableView.dataSource = self
        myTableView.delegate = self
        
        myTableView.registerClass(HistoryCutomCell.classForCoder(), forCellReuseIdentifier: "MyCell")
        myTableView.tag = 99999
        
        // Viewに追加する.
        let parent = self.view.superview
        self.view.addSubview(currentMonthView)
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        //super.touchesEnded(touches as Set<NSObject>, withEvent: event)
        let rect = UIScreen.mainScreen().applicationFrame
        for touch: AnyObject in touches {
            var t: UITouch = touch as! UITouch
            if(t.view.tag > 10000){
                let dateStr:String = String(t.view.tag)
                var year = (dateStr as NSString).substringWithRange(NSRange(location: 0, length: 4))
                var month = (dateStr as NSString).substringWithRange(NSRange(location: 4, length: 2))
                var day = (dateStr as NSString).substringWithRange(NSRange(location: 6, length: 2))
                self.section = year + "-" + month + "-" + day
                var resultActs:NSMutableArray = self.getHistory(year.toInt()!, month: month.toInt()!, day: day.toInt()!)
                if(resultActs.count > 0){
                    self.view.addSubview(self.myTableView)
                    self.view.bringSubviewToFront(self.myTableView)
                    UIView.animateWithDuration(0.3, animations: {() -> Void in
                    self.myTableView.frame = CGRectMake(0, (rect.height*2/3)-20-40, rect.width, rect.height / 3)
                        }, completion:nil )
                    self.myItems = []
                    self.Types = []
                    for resultAct in resultActs{
                        var res:History = resultAct as! History
                        self.myItems += [res.content]
                        self.Types.append(Int(res.type))
                    }
                    self.myTableView.reloadData()
                }else{
                    self.myTableView.reloadData()
                    self.view.bringSubviewToFront(self.myTableView)
                    UIView.animateWithDuration(0.3, animations: {() -> Void in
                        self.myTableView.frame = CGRectMake(0, CGFloat(rect.height+50) , rect.width, rect.height)
                        }, completion: {(Bool) -> Void in
                            self.myTableView.removeFromSuperview()
                        }
                    )
                }
            }
        }
    }
    
    /*
    Cellが選択された際に呼び出される.
    */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    /*
    Cellの総数を返す.
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myItems.count
    }
    
    /*
    Cellに値を設定する.
    */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var subviews = self.view.subviews
        // サブビューを取り除く
        var cell:HistoryCutomCell = myTableView.dequeueReusableCellWithIdentifier("MyCell") as! HistoryCutomCell

        cell.configureCell(myItems[indexPath.row], type: Types[indexPath.row], atIndexPath : indexPath)
        var cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = cellSelectedBgView
        // Cellに値を設定する.
        return cell
    }
    
    /*
    セクションの数を返す.
    */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        return nil;
    }

    /*
    セクションのタイトルを返す.
    */
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.section
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView,
        forSection section: Int) {
            let rect = UIScreen.mainScreen().applicationFrame
            let font = UIFont(name: "HelveticaNeue-Bold", size: 15)
            // Text Color/Font
            let header = view as! UITableViewHeaderFooterView
            header.textLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
            header.textLabel.font = font // put the font you want here...
            
            let image = UIImage(named: "stop32.png")
            let imageButton   = UIButton()
            
            //サイズ
            imageButton.frame = CGRectMake(rect.width-50 , 4, 22, 22)
            imageButton.setImage(image, forState: .Normal)
            
            //ボタンをタップした時に実行するメソッドを指定
            imageButton.addTarget(self, action: "tapped:", forControlEvents:.TouchUpInside)
            header.addSubview(imageButton)
            header.bringSubviewToFront(imageButton)
    }
    
    func tapped(sender: AnyObject) {
        let rect = UIScreen.mainScreen().applicationFrame
        UIView.animateWithDuration(0.3, animations: {() -> Void in
            self.myTableView.frame = CGRectMake(0, CGFloat(rect.height+50), rect.width, rect.height)
            }, completion: {(Bool) -> Void in
                self.myTableView.removeFromSuperview()
        })
    }
    func getHistory(year:Int, month:Int, day:Int) -> NSMutableArray {
        var resultArray:NSMutableArray = []
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "History")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        //ORDER BY を指定
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var dateString = String(format:"%d-%d-%d", year, month, day);
        var dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        var d: NSDate = dateStringFormatter.dateFromString(dateString)!
        
        var comp = NSDateComponents()
        comp.day = 1
        let calendar = NSCalendar.currentCalendar()
        var nD:NSDate = calendar.dateByAddingComponents(comp, toDate:d, options: nil)!
        
        fetchRequest.predicate =  NSPredicate(format: "createdAt >= %@ AND createdAt < %@", d, nD)
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [History]!
        for result in results {
            resultArray.addObject(result)
        }
        return resultArray
    }
}
