//
//  DayView.swift
//  pms
//
//  Created by soroban11 on 2015/02/19.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit
import CoreData

class DayView: UIView {
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    init(frame:CGRect,year:Int,month:Int,day:Int,weekday:Int, dayWidth:CGFloat, dayHeight:CGFloat, gray:Bool){
        super.init(frame: frame)
        var dayLabel:UILabel = UILabel(frame: CGRectMake(0, 0, dayWidth, dayHeight))
        dayLabel.textAlignment = NSTextAlignment.Left
        
        var dateLabelStr:String = String(format:" %02d\n\n\n\n", day)
        if weekday == 1 {
            //日曜日は赤
            dayLabel.textColor = UIColor.redColor()
        } else if weekday == 7 {
            //土曜日は青
            dayLabel.textColor = UIColor.blueColor()
        }else{
            dayLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
        }
        dayLabel.numberOfLines = 6
        dayLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 8)
        
        // ボーダーの色
        dayLabel.layer.borderColor = UIColor(red:0.68, green:0.68, blue:0.69, alpha:0.9).CGColor
        if(gray){
            dayLabel.backgroundColor = UIColor(red:0.89, green:0.89, blue:0.9, alpha:0.6)
        }
        else{
            var tagStr:String = String(format: "%04d%02d%02d", year,month,day)
            dayLabel.tag = tagStr.toInt()!
        }
        
        // ボーダーの太さ
        dayLabel.layer.borderWidth = 0.5
        // 丸角
        dayLabel.layer.cornerRadius = 0
        dayLabel.userInteractionEnabled = true;
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd" // 日付フォーマットの設定
        var dateString: String = dateFormatter.stringFromDate(NSDate())
        var dates:[String] = dateString.componentsSeparatedByString("/")
        
        var currentYear: Int  = dates[0].toInt()!
        var currentMonth: Int = dates[1].toInt()!
        var currentDay: Int  = dates[2].toInt()!
        
        dayLabel.text = dateLabelStr
        if(currentYear == year && currentMonth == month && currentDay == day){
            dayLabel.backgroundColor = UIColor(red:0.99, green:0.94, blue:0.95, alpha:1.0)
        }
        
        self.addSubview(dayLabel)
        
        var resultAct:NSMutableArray = self.getHistory(year, month: month, day: day)
        
        var num:Int = resultAct.count
        for (var i = 0; i <= 1; i++ ) {
            if(num > i){
                var res:History = resultAct[i] as! History
                self.getHistoryExit(year, month: month, day: day, type:i)
                var height = (Int(dayHeight)/5) + (12*(i+1))+5
                var activeLabel:UILabel = UILabel(frame: CGRectMake(2, CGFloat(height), CGFloat(dayWidth-2), 9))
                activeLabel.text = res.content
                activeLabel.lineBreakMode = NSLineBreakMode.ByClipping
                activeLabel.numberOfLines = 1
                activeLabel.font = UIFont(name: "HelveticaNeue", size: 8)
                if(res.type == 2){
                    activeLabel.backgroundColor = UIColor(red:0.84, green:0.88, blue:0.95, alpha:1.0)
                }else{
                    activeLabel.backgroundColor = UIColor(red:0.95, green:0.84, blue:0.88, alpha:1.0)
                }
                self.addSubview(activeLabel)
            }
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesEnded(touches as Set<NSObject>, withEvent: event)
        for touch: AnyObject in touches {
            var t: UITouch = touch as! UITouch
            if(t.view.tag > 10000){
                var label:UILabel = t.view.viewWithTag(t.view.tag) as! UILabel
                label.backgroundColor = UIColor(red:0.99, green:0.94, blue:0.95, alpha:1.0)
            }
        }
    }
    
    func getHistory(year:Int, month:Int, day:Int) -> NSMutableArray {
        var resultArray:NSMutableArray = []
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "History")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        //ORDER BY を指定
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var dateString = String(format:"%d-%d-%d", year, month, day);
        var dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        var d: NSDate = dateStringFormatter.dateFromString(dateString)!

        var comp = NSDateComponents()
        comp.day = 1
        let calendar = NSCalendar.currentCalendar()
        var nD:NSDate = calendar.dateByAddingComponents(comp, toDate:d, options: nil)!
        
        fetchRequest.predicate =  NSPredicate(format: "createdAt >= %@ AND createdAt < %@", d, nD)
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [History]!
        for result in results {
            resultArray.addObject(result)
        }
        return resultArray
    }
    
    func getHistoryExit(year:Int, month:Int, day:Int, type:Int) -> Bool {
        var resultArray:NSMutableArray = []
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "History")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        //ORDER BY を指定
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var dateString = String(format:"%d-%d-%d", year, month, day);
        var dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        var d: NSDate = dateStringFormatter.dateFromString(dateString)!
        
        var comp = NSDateComponents()
        comp.day = 1
        let calendar = NSCalendar.currentCalendar()
        var nD:NSDate = calendar.dateByAddingComponents(comp, toDate:d, options: nil)!
        
        fetchRequest.predicate =  NSPredicate(format: "createdAt >= %@ AND createdAt < %@ AND type == %d", d, nD, type)
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [History]!
        var returnValue:Bool = false
        if(results.count > 0){
            returnValue = true
        }
        return returnValue
    }

}