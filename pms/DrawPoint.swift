//
//  DrawPoint.swift
//  pms
//
//  Created by soroban on 2014/12/16.
//  Copyright (c) 2014年 soroban. All rights reserved.
//

import UIKit
import CoreData
import QuartzCore

class DrawCircle: UIView {
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    var nMile:Int!
    var nPercent:Double = 0
    //下地の円弧を描くのにかける時間
    var underDuration:Double = 0.5
    //上側の扇を描くのにかける時間
    var overDuration:Double = 0.0
    
    var radius:CGFloat = 0
    var lineWigth:CGFloat = 0
    var under:CGFloat = 0
    var unit:CGFloat = 0
    var pointLabel:CGFloat = 0
    var appNameJa:CGFloat = 0
    var appNameEn:CGFloat = 0
    var posX:CGFloat = 0
    //カウント用のポイント
    var countMile:Int = 0
    var countFlag1:Bool = false
    var countFlag2:Bool = false
    var countFlag3:Bool = false
    var countFlag4:Bool = false
    var countFlag5:Bool = false
    // ポイント(数字)を表示するUITextFieldを作成する.
    let pnumTextView:UILabel = UILabel(frame: CGRectMake(0,0,130,50))
    let punitTextView: UILabel = UILabel(frame: CGRectMake(0,0,250,50))
    let pmsTextView: UILabel = UILabel(frame: CGRectMake(0,0,250,50))
    var per:CGFloat = 5
    //フォーマター
    var formatter = NSNumberFormatter()
    //タイマー
    var oCircleTimer : NSTimer!
    var cPointTimer : NSTimer!
    var updater: CADisplayLink!
    var countArr: [Int] = []
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        let size = UIScreen.mainScreen().bounds.size
        let scale = UIScreen.mainScreen().scale
        let result = CGSizeMake(size.width * scale, size.height * scale)
        if(result.height <= 960){
            self.radius = 90
            self.lineWigth = 40
            self.under = 40
            self.unit = 10
            self.pointLabel = 25
            self.appNameJa = 11
            self.appNameEn = 9
            self.posX = 50
        }else{
            self.radius = 110
            self.lineWigth = 45
            self.under = 49
            self.unit = 14
            self.pointLabel = 30
            self.appNameJa = 14
            self.appNameEn = 12
            self.posX = 63
        }
        
        self.nMile = self.getPoint()

        if(self.exitAff()){
            per = 4.5
        }
        self.stopUpdater()
        nPercent = Double(nMile)/Double(app.maxMile)
        
        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3

        // 表示する文字を代入する.
        pnumTextView.text = "0"
        pnumTextView.textAlignment = .Center
        // 背景を透明にする
        pnumTextView.opaque = false;
        pnumTextView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        pnumTextView.textColor = UIColor(red: 0.17, green: 0.17, blue: 0.17, alpha: 1.0)
        
        // UITextFieldの表示する位置を設定する.
        pnumTextView.layer.position = CGPoint(x:(app.screenWidth/2), y:(app.screenHeight*per/10)+13-self.under );
        // フォントの設定をする.
        pnumTextView.font = UIFont(name:"HelveticaNeue",size:self.pointLabel)
        // Viewに追加する.
        self.addSubview(pnumTextView)
        
        // ポイント(数字)を表示するUITextFieldを作成する.
        // 表示する文字を代入する.
        punitTextView.text = "point"
        punitTextView.textAlignment = .Center
        // 背景を透明にする
        punitTextView.opaque = false;
        punitTextView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        punitTextView.textColor = UIColor(red: 0.17, green: 0.17, blue: 0.17, alpha: 1.0)
        
        // UITextFieldの表示する位置を設定する.
        punitTextView.layer.position = CGPoint(x:(app.screenWidth/2)+self.posX, y:(app.screenHeight*per/10)+16-self.under );
        // フォントの設定をする.
        punitTextView.font = UIFont(name:"HelveticaNeue",size:self.unit)
        
        // ポイント(数字)を表示するUITextFieldを作成する.
        // 表示する文字を代入する.
//        pmsTextView.text = "Private.\n                  Mileage.\n                              System."
        pmsTextView.text = NSLocalizedString("id00045",comment: "")
        pmsTextView.textAlignment = .Center
        // 背景を透明にする
        pmsTextView.opaque = false;
        pmsTextView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        pmsTextView.textColor = UIColor(red: 0.17, green: 0.17, blue: 0.17, alpha: 1.0)
        
        // UITextFieldの表示する位置を設定する.
        pmsTextView.layer.position = CGPoint(x:(app.screenWidth/2), y:(app.screenHeight*per/10)-18-self.under );
        // フォントの設定をする.
        let countryCode = NSLocale.preferredLanguages().first as! String
        if(countryCode == "ja"){
            pmsTextView.font = UIFont(name:"HelveticaNeue",size:self.appNameJa)
        }else{
            pmsTextView.font = UIFont(name:"HelveticaNeue",size:self.appNameEn)
        }
        // Viewに追加する.
        self.addSubview(pmsTextView)
        
        //現在の取得ポイント1++でカウントアップすると時間かかるため一桁づつカウントアップ
        //そのため一旦、各桁毎に分解
        var countStr:String = "\(nMile)"
        var countA = count(countStr)
        for (var i = 0, n = 5; i < n ; i++ ){
            if(i < countA){
                let index = advance(countStr.startIndex, i)
                countArr.append(String(countStr[index]).toInt()!)
            }else{
                countArr.append(0)
            }
        }
        
        //カウントにかかる時間を計算
        for (var i = 0, n = 5; i < n ; i++ ){
            overDuration = overDuration + Double(countArr[i] + 1) * 0.022
        }
        
        self.drawUnderCircle();
        oCircleTimer = NSTimer.scheduledTimerWithTimeInterval(underDuration + 0.1, target: self, selector: Selector("drawOverCircle"), userInfo: nil, repeats: false)

        if(self.nMile > 0){
            cPointTimer = NSTimer.scheduledTimerWithTimeInterval(underDuration + 0.1, target: self, selector: Selector("countPoint"), userInfo: nil, repeats: false)
        }
        self.addSubview(punitTextView)
    }
    
    //下側の円弧
    func drawUnderCircle(){
        var circlePath = UIBezierPath(arcCenter: CGPoint(x:app.screenWidth/2, y:(app.screenHeight*per/10) - self.under ), radius: self.radius,  startAngle: 0 - (2*CGFloat(M_PI))*1/4, endAngle: (CGFloat(M_PI)*2)*1.0 - (2*CGFloat(M_PI))*1/4, clockwise: true)
        
        // CAShapeLayerへ描く弧を設定
        var circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.CGPath
        circleLayer.fillColor = UIColor.clearColor().CGColor
        circleLayer.strokeColor = UIColor(red: 0.61, green: 0.15, blue: 0.17, alpha: 1.0).CGColor
        circleLayer.lineWidth = self.lineWigth;
        
        // 弧を描くlayerをsublayerへ追加
        layer.addSublayer(circleLayer)
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = underDuration;
        
        // アニメーション 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = 1
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        circleLayer.strokeEnd = 2.0
        
        // アニメーションを実行
        circleLayer.addAnimation(animation, forKey: "animateCircle")
    }
    
    //上側の扇型の描写
    func drawOverCircle(){
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        //外側の円
        var pointPath = UIBezierPath(arcCenter: CGPoint(x:app.screenWidth/2, y:(app.screenHeight*per/10)-self.under ), radius: self.radius,  startAngle: 0 - (2*CGFloat(M_PI))*1/4, endAngle: ((CGFloat(M_PI)*2)*CGFloat(nPercent)) - (2*CGFloat(M_PI))*1/4, clockwise: true)
        
        // CAShapeLayerへ描く弧を設定
        var pointLayer = CAShapeLayer()
        pointLayer.path = pointPath.CGPath
        pointLayer.fillColor = UIColor.clearColor().CGColor
        pointLayer.strokeColor = UIColor(red: 0.96, green: 0.12, blue: 0.10, alpha: 1.0).CGColor
        pointLayer.lineWidth = self.lineWigth;
        
        // 弧を描くlayerをsublayerへ追加
        layer.addSublayer(pointLayer)
        animation.duration = overDuration + 0.05;
        
        // アニメーション 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = 1
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        pointLayer.strokeEnd = 1.0
        
        // アニメーションを実行
        pointLayer.addAnimation(animation, forKey: "animatePoint")
    }
    
    func countPoint(){
        countMile = 0;
        countFlag1 = false
        countFlag2 = false
        countFlag3 = false
        countFlag4 = false
        countFlag5 = false
        
        updater = CADisplayLink(target: self, selector:Selector("countUp"))
        updater.frameInterval = 0
        updater.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)

    }
    
    //ポイントのテキストのカウント
    func countUp(){
        pnumTextView.text = formatter.stringFromNumber(countMile++)
        if(countMile == nMile){
            pnumTextView.text = formatter.stringFromNumber(countMile)
            updater.removeFromRunLoop(NSRunLoop.currentRunLoop(), forMode: NSRunLoopCommonModes)
        }
        else if(countMile >= countArr[0]){
            if(countFlag1 == false){
                countMile = Int(countArr[0] * 10)
                countFlag1 = true
            }
            if(countMile == nMile){
                pnumTextView.text = formatter.stringFromNumber(countMile)
                updater.invalidate()
            }
            else if(countMile >= Int(countArr[0] * 10) + countArr[1]){
                var judgeNum = Int(countArr[0] * 100) + Int(countArr[1] * 10) + Int(countArr[2]);
                if(countFlag2 == false){
                    countMile = Int(countArr[0] * 100) + Int(countArr[1] * 10)
                    countFlag2 = true
                }
                if(countMile == nMile){
                    pnumTextView.text = formatter.stringFromNumber(countMile)
                    updater.invalidate()
                }
                else if(countMile >= judgeNum){
                    var judgeNum2 = Int(countArr[0]*1000) + Int(countArr[1] * 100) + Int(countArr[2] * 10) + Int(countArr[3])
                    if(countFlag3 == false){
                        countMile = Int(countArr[0] * 1000) + Int(countArr[1] * 100) + Int(countArr[2] * 10)
                        countFlag3 = true
                    }
                    if(countMile == nMile){
                        pnumTextView.text = formatter.stringFromNumber(countMile)
                        updater.invalidate()
                    }
                    else if(countMile >= judgeNum2){
                        if(countFlag4 == false){
                            countMile = Int(countArr[0] * 10000) + Int(countArr[1] * 1000) + Int(countArr[2] * 100) + Int(countArr[3] * 10)
                            countFlag4 = true
                        }
                        if(countMile == nMile){
                            pnumTextView.text = formatter.stringFromNumber(countMile)
                            updater.invalidate()
                        }
                        else if(countMile >= Int(countArr[0] * 10000) + Int(countArr[1] * 1000) + Int(countArr[2] * 100) + Int(countArr[3] * 10) + countArr[4]){
                            if(countFlag5 == false){
                                countMile = Int(countArr[0] * 100000) + Int(countArr[1] * 10000) + Int(countArr[2] * 1000) + Int(countArr[3] * 100) + Int(countArr[4] * 10)
                                countFlag5 = true
                            }
                            if(countMile == nMile){
                                pnumTextView.text = formatter.stringFromNumber(countMile)
                                updater.invalidate()
                            }
                            else if(countMile >= app.maxMile){
                                pnumTextView.text = formatter.stringFromNumber(app.maxMile)
                                updater.invalidate()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func stopUpdater(){
        if(updater != nil){
            updater.invalidate()
        }
    }
    
    func getPoint()->Int {
        var point:Int = 0
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Point")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Point]!
        
        for result in results {
            point = Int(result.point)
        }
        return point
    }
    func exitAff() -> Bool{
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Affirmation")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        var res: Affirmation!
        //ORDER BY を指定
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Affirmation]!
        if(results.count > 0){
            return true
        }else{
            return false
        }
    }
}
