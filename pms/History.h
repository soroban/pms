//
//  History.h
//  pms
//
//  Created by soroban on 2014/12/05.
//  Copyright (c) 2014年 soroban. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface History : NSManagedObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * createdAt;
@property (nonatomic, retain) NSNumber * point;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * type;

@end
