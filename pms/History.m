//
//  History.m
//  pms
//
//  Created by soroban on 2014/12/05.
//  Copyright (c) 2014年 soroban. All rights reserved.
//

#import "History.h"


@implementation History

@dynamic content;
@dynamic createdAt;
@dynamic point;
@dynamic id;
@dynamic type;

@end
