//
//  History.swift
//  pms
//
//  Created by soroban11 on 2015/02/17.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import Foundation
import CoreData

class History: NSManagedObject {

    @NSManaged var content: String
    @NSManaged var createdAt: NSDate
    @NSManaged var id: NSNumber
    @NSManaged var point: NSNumber
    @NSManaged var type: NSNumber
    
    
}
