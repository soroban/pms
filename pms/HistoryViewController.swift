//
//  HistoryViewController.swift
//  pms
//
//  Created by soroban11 on 2015/02/19.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit
import SpriteKit
import CoreData

class HistoryViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource{
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    @IBOutlet var navigationPageIndicator: UIPageControl!
    var pageData:NSMutableArray = []
    var mon = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",]
    var navBarHeight:CGFloat!
    var tabBarHeight:CGFloat!
    var _pageViewController: UIPageViewController? = nil
    var dateLabel: UILabel!
    var pageViewController: UIPageViewController {
        if _pageViewController == nil {
            _pageViewController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.Scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.Horizontal, options: nil)
            _pageViewController?.delegate = self
            _pageViewController?.dataSource = self
            }
            return _pageViewController!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navBarHeight = self.navigationController?.navigationBar.frame.size.height
        let statusBarHeight = UIApplication.sharedApplication().statusBarFrame.size.height
        self.navBarHeight = self.navBarHeight+statusBarHeight
        self.tabBarHeight = self.tabBarController!.tabBar.frame.size.height
        
        let font = UIFont(name: "HelveticaNeue-Bold", size: 12)

        
        var weeks:[String]=["日","月","火","水","木","金","土"]
        var weeks_en:[String]=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
        
        let v:UIView = UIView(frame:CGRectMake(0, 0, app.screenWidth, 70));
        
        let countryCode = NSLocale.preferredLanguages().first as! String
        if(countryCode == "ja"){
            var key:Int = 0
            for week in weeks{
                var dayWidth:Int = Int(app.screenWidth / 7.0)
                var frame:CGRect = CGRectMake(CGFloat((key * dayWidth) - 8),
                    CGFloat(30),
                    CGFloat(dayWidth),
                    CGFloat(30))

            
                let label: UILabel = UILabel(frame: frame)
                if key == 0 {
                    //日曜日は赤
                    label.textColor = UIColor.redColor()
                } else if key == 6 {
                    //土曜日は青
                    label.textColor = UIColor.blueColor()
                }else{
                    label.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
                }

                label.textAlignment = NSTextAlignment.Center
                label.text = week
                label.font = font
                v.addSubview(label)
                key++
            }
        }else{
            var key:Int = 0
            for week in weeks_en{
                var dayWidth:Int = Int(app.screenWidth / 7.0)
                var frame:CGRect = CGRectMake(CGFloat((key * dayWidth) - 8),
                    CGFloat(30),
                    CGFloat(dayWidth),
                    CGFloat(30))
                
                
                let label: UILabel = UILabel(frame: frame)
                if key == 0 {
                    //日曜日は赤
                    label.textColor = UIColor.redColor()
                } else if key == 6 {
                    //土曜日は青
                    label.textColor = UIColor.blueColor()
                }else{
                    label.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
                }
                
                label.textAlignment = NSTextAlignment.Center
                label.text = week
                label.font = font
                v.addSubview(label)
                key++
            }
        }
        var dateFrame:CGRect = CGRectMake(CGFloat(0),
            CGFloat(0),
            CGFloat(app.screenWidth),
            CGFloat(40))
        
        self.dateLabel = UILabel(frame: dateFrame)
        self.dateLabel.textAlignment = NSTextAlignment.Center
        self.dateLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
        self.dateLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
        v.addSubview(self.dateLabel)
        self.navigationItem.titleView = v
        
        let dateFormatter = NSDateFormatter()
        let calendar = NSCalendar.currentCalendar()
        var comp = NSDateComponents()
        
        var startDay:Int = 0
        for ( var i = -10*12, n = 10*12 ; i <= n ; i++ ) {
            comp.month = i
            let d = calendar.dateByAddingComponents(comp, toDate: NSDate(), options: nil)!
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/01" // 日付フォーマットの設定
            self.pageData.addObject(dateFormatter.stringFromDate(d))
        }
        
        self.navigationPageIndicator.numberOfPages = self.pageData.count

        let startingViewController = self.viewControllerAtIndex(120, storyboard: self.storyboard!)!
        self.currentPageIndicator(120)
        self.changeNavi(120);
        
        var weakRef:UIPageViewController = self.pageViewController
        let viewControllers: NSArray = [startingViewController]
        self.pageViewController.setViewControllers(viewControllers as! [AnyObject], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        let pageViewRect = self.view.bounds
        self.pageViewController.view.frame = pageViewRect
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
        self.view.gestureRecognizers = self.pageViewController.gestureRecognizers
        
        var dateString: String = self.pageData[120].description
        var dates:[String] = dateString.componentsSeparatedByString("/")
        var currentYear: Int  = dates[0].toInt()!
        var currentMonth: Int = dates[1].toInt()!
        
        //self.dateLabel.text = NSString(format:"%d月 %d年", currentMonth, currentYear)
    }
    
    override func viewDidAppear(animated: Bool) {
        var minDate:NSDate = getHistoryDate(true)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM" // 日付フォーマットの設定
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Private methods
    func viewControllerAtIndex(index: Int, storyboard: UIStoryboard) -> DataViewController? {
        if self.pageData.count == 0 || (index >= self.pageData.count) {
            return nil
        }
        let dataViewController: DataViewController = storyboard.instantiateViewControllerWithIdentifier("DataViewController") as! DataViewController
        dataViewController.dateObject = self.pageData[index]
        dataViewController.navBarHeight = self.navBarHeight
        dataViewController.tabBarHeight = self.tabBarHeight
        return dataViewController
    }
    func indexOfViewController(viewController: DataViewController) -> Int {
        if let dataObject: AnyObject = viewController.dateObject {
            return self.pageData.indexOfObject(dataObject)
        } else {
            return NSNotFound
        }
    }
    func currentPageIndicator(index: Int) {
        self.navigationPageIndicator.currentPage = index
    }
    // MARK: - UIPageViewController delegate methods
    func pageViewController(pageViewController: UIPageViewController, spineLocationForInterfaceOrientation orientation: UIInterfaceOrientation) -> UIPageViewControllerSpineLocation {
        let currentViewController = self.pageViewController.viewControllers[0] as! UIViewController
        let viewControllers: NSArray = [currentViewController]
        var weakRef:UIPageViewController = self.pageViewController
        self.pageViewController.setViewControllers(viewControllers as! [AnyObject], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        self.pageViewController.doubleSided = false
        return UIPageViewControllerSpineLocation.Min
    }
    // MARK: - UIPageViewController Data Source
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! DataViewController)
        self.currentPageIndicator(index)
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        index--
        return self.viewControllerAtIndex(index, storyboard: viewController.storyboard!)
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! DataViewController)
        if index == NSNotFound {
            return nil
        }
        self.currentPageIndicator(index)
        index++
        if index == self.pageData.count {
            return nil
        }
        return self.viewControllerAtIndex(index, storyboard: viewController.storyboard!)
    }
    
    func getHistoryDate(order:Bool)-> NSDate {
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "History")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.fetchLimit = 1

        //ORDER BY を指定
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: order)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = [sortDescriptor]
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [History]!
        for result in results {
            //self.myValues.addObject(result)
            return result.createdAt
        }
        return NSDate()
    }
    
    func changeNavi(index:Int){
        var dateString: String = self.pageData[index].description
        var dates:[String] = dateString.componentsSeparatedByString("/")
        var currentYear: Int  = dates[0].toInt()!
        var currentMonth: Int = dates[1].toInt()!
        let countryCode = NSLocale.preferredLanguages().first as! String
        if(countryCode == "ja"){
            self.dateLabel.text = NSString(format:"%d月  %d年", currentMonth, currentYear) as? String
        }else{
            self.dateLabel.text = NSString(format:"%@  %d", mon[currentMonth-1], currentYear) as? String
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController,
        willTransitionToViewControllers pendingViewControllers: [AnyObject]){
            pageViewController
            var index:Int = 0
            let pageContentViewController = pendingViewControllers[0] as! DataViewController
            if(pendingViewControllers.count > 0)
            {
                index = self.indexOfViewController(pageContentViewController as DataViewController)
            }
            self.changeNavi(index);
    }
}