//
//  IconDetailViewController.swift
//  pms
//
//  Created by soroban11 on 2015/04/01.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

class IconDetailViewController: UIViewController, UIActionSheetDelegate {
    @IBOutlet var myTableView: UITableView!
    var myItems = ["icons8.com", "flaticon"]
    var urls = ["http://icons8.com/", "http://www.flaticon.com/"]
    var mySections = [NSLocalizedString("id00048",comment: "")]
    var url:String!
    var labelURL: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let attrText = NSMutableAttributedString(string: myItems[indexPath.row]+" ("+urls[indexPath.row]+")")
        if(indexPath.row == 0){
        attrText.addAttribute(NSForegroundColorAttributeName,
            value: UIColor(red:0, green:0.58, blue:0.85, alpha:1.0),
            range: NSMakeRange(12, 18))
        }else{
            attrText.addAttribute(NSForegroundColorAttributeName,
                value: UIColor(red:0, green:0.58, blue:0.85, alpha:1.0),
                range: NSMakeRange(10, 24))
        }
        let font = UIFont(name: "HelveticaNeue", size: 16)
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        cell.selectionStyle = UITableViewCellSelectionStyle.Blue
        cell.textLabel?.attributedText = attrText
        cell.textLabel?.font = font
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if objc_getClass("UIAlertController") != nil {
            var alertController = UIAlertController(title: "", message: self.urls[indexPath.row], preferredStyle: .ActionSheet)
            
            let openAction = UIAlertAction(title: NSLocalizedString("id00050",comment: ""), style: .Default) {
                action in self.openUrl(self.urls[indexPath.row])
            }
            let cancelAction = UIAlertAction(title: NSLocalizedString("id00009",comment: ""), style: .Cancel, handler: nil)
            
            alertController.addAction(openAction)
            alertController.addAction(cancelAction)
            presentViewController(alertController, animated: true, completion: nil)
        }else{
            var sheet: UIActionSheet = UIActionSheet()
            sheet.title  = self.urls[indexPath.row]
            self.url = self.urls[indexPath.row]
            sheet.delegate = self
            sheet.addButtonWithTitle(NSLocalizedString("id00050",comment: ""))
            sheet.addButtonWithTitle(NSLocalizedString("id00009",comment: ""))
            sheet.cancelButtonIndex = 1
            sheet.showInView(self.view)
        }
    }

    /*
    セクションの数を返す.
    */
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return mySections.count
    }
    
    /*
    セクションのタイトルを返す.
    */
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return mySections[section]
    }
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if (view.isKindOfClass(UITableViewHeaderFooterView)) {
            var tableViewHeaderFooterView:UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
            tableViewHeaderFooterView.textLabel.text = mySections[section]
        }
    }

    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return myItems.count
    }

    func actionSheet(sheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        switch (buttonIndex) {
        case 0:
            self.openUrl(self.url)
            break;
        default:
            break
        }
    }
    
    func openUrl(url:String!) {
        let targetURL=NSURL(string: url)
        let application=UIApplication.sharedApplication()
        application.openURL(targetURL!)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
