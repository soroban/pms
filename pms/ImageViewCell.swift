//
//  imageViewCell.swift
//  pms
//
//  Created by soroban11 on 2015/04/21.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

class ImageViewCell: UITableViewCell {
    var myImageView:UIImageView!
    var myView:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(atIndexPath indexPath: NSIndexPath){
        self.myView = UIView(frame: CGRectMake(0,0,UIScreen.mainScreen().applicationFrame.width, 380))

        self.myImageView = UIImageView()
    }
    
    func addSub(){
        self.myView.addSubview(self.myImageView)
        self.addSubview(self.myImageView)
    }
}
