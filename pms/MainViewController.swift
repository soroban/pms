//
//  MainViewController.swift
//  pms
//
//  Created by soroban on 2014/12/05.
//  Copyright (c) 2014年 soroban. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class MainViewController: UIViewController {
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    var myValues = NSMutableArray()
    var underDuration:Double = 0.5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let v:UIView = UIView(frame:CGRectMake(0, 0, app.screenWidth, 70));
        var frame:CGRect = CGRectMake(CGFloat(0),
            CGFloat(0),
            CGFloat(app.screenWidth+10),
            CGFloat(70))
        
        var topLabel = UILabel(frame: frame)
        topLabel.textAlignment = NSTextAlignment.Center
        topLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        topLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
        topLabel.text = NSLocalizedString("id00055",comment: "")
        v.addSubview(topLabel)
        
        var rect:CGRect = CGRectMake((app.screenWidth/2)-60, 25, 25, 25);
        let countryCode = NSLocale.preferredLanguages().first as! String
        if(countryCode == "ja"){
            var rect:CGRect = CGRectMake((app.screenWidth/2)-50, 25, 25, 25);
        }
        var image = UIImage(named: "airplane_takeoff.png")
        var imageView = UIImageView(image: image)
        imageView.frame = rect;
        v.addSubview(imageView)
        
        self.navigationItem.titleView = v
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(false)
        
        self.myValues = []
        self.getAff()
        //画面描写
        //描画範囲を指定
        var mainDraw = DrawCircle(frame: CGRectMake(0, 0, self.app.screenWidth, self.app.screenHeight))
        //背景を指定
        mainDraw.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0)
        //追加
        var pointTimer = NSTimer.scheduledTimerWithTimeInterval(underDuration + 0.1, target: self, selector: Selector("drawLabel"), userInfo: nil, repeats: false)

        self.view.addSubview(mainDraw)
    }
    
    func drawLabel(){
        var exist = exitAff()
        if(exist){
            var aff = self.myValues[0] as! Affirmation
            if(aff.dispFlg){
                var myLabel:UILabel =  UILabel(frame: CGRectMake(0, self.app.screenHeight, self.app.screenWidth, self.app.screenHeight/5))
                var labelText = ""
                myLabel.text = ""
                for value in myValues{
                    var aff = value as! Affirmation
                    if(labelText.isEmpty){
                        labelText = aff.aim
                    }else{
                        labelText = labelText + "\n" + aff.aim
                    }
                }
                let attributedText = NSMutableAttributedString(string: labelText)
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 10
                attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attributedText.length))
                
                //textではなく、attributedTextを使う
                myLabel.attributedText = attributedText
                myLabel.textAlignment = NSTextAlignment.Center
                myLabel.numberOfLines = myValues.count
                myLabel.font = UIFont(name: "HelveticaNeue", size: 13)
                //            myLabel.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.94, alpha:1.0)
                //myLabel.adjustsFontSizeToFitWidth = YES;
                
                UIView.animateWithDuration(0.7, animations: {() -> Void in
                    myLabel.frame = CGRectMake(0, self.app.screenHeight*7/10 - 49, self.app.screenWidth, self.app.screenHeight/6)
                    }, completion: nil)
                
                self.view.addSubview(myLabel)
            }
        }
    }
    override func viewWillDisappear(animated: Bool){
        super.viewWillAppear(false);
        var subviews = self.view.subviews
        for subview in subviews {
            subview.removeFromSuperview()
        }
        super.viewWillAppear(false);
        var mainDraw = DrawCircle(frame: CGRectMake(0, 0, self.app.screenWidth, self.app.screenHeight/5))
        
        mainDraw.stopUpdater()
    }
    
    func exitAff() -> Bool{
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Affirmation")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        var res: Affirmation!
        //ORDER BY を指定
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Affirmation]!
        if(results.count > 0){
            return true
        }else{
            return false
        }
    }
    
    func getAff(){
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Affirmation")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        var res: Affirmation!
        //ORDER BY を指定
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Affirmation]!
        for result in results {
            self.myValues.addObject(result)
        }
    }
}
