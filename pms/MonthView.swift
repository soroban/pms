//
//  MonthView.swift
//  pms
//
//  Created by soroban11 on 2015/02/19.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

class MonthView: UIView {
    var selTag:Int = 999999
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    init(frame: CGRect,year:Int,month:Int) {
        super.init(frame:frame)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd" // 日付フォーマットの設定
        var dateString: String = dateFormatter.stringFromDate(NSDate())
        var dates:[String] = dateString.componentsSeparatedByString("/")
        
        var currentYear: Int  = dates[0].toInt()!
        var currentMonth: Int = dates[1].toInt()!
        var currentDay: Int  = dates[2].toInt()!
        
        if(currentYear == year && currentMonth == month){
            var selTagStr = String(format: "%04d%02d%02d", currentYear,currentMonth,currentDay)
            self.selTag = selTagStr.toInt()!
        }else{
            self.selTag = 999999
        }
        self.setUpDays(year,month:month)
    }
    
    func setUpDays(year:Int,month:Int){
        var subviews = self.subviews
        for subview in subviews {
            subview.removeFromSuperview()
        }

        var subViews:[UIView] = self.subviews as! [UIView]
        for view in subViews {
            if view.isKindOfClass(DayView) {
                view.removeFromSuperview()
            }
        }
        var dayWidth:CGFloat = frame.size.width / 7.0
        var dayHeight:CGFloat = (frame.size.height-49) / 6.0
        
        var nextMonth:Int = month+1
        var beforeMonth:Int = month-1
        var nextYear:Int = year
        var beforeYear:Int = year

        if(month == 12){
            nextMonth = 1
            beforeMonth = 11
            nextYear = year+1
            beforeYear = year
        }else if(month == 1){
            nextMonth = 2
            beforeMonth = 12
            nextYear = year
            beforeYear = year-1
        }
        
        var lastDay:Int? = self.getLastDay(beforeYear,month:beforeMonth)
        if lastDay != nil {
            //初日の曜日を取得
            var weekday:Int = self.getWeekDay(year,month: month,day:1)
            var lastWeekday:Int = self.getWeekDay(beforeYear, month: beforeMonth,day:lastDay!-weekday+2)
            
            for var i:Int = lastDay!-weekday+1; i < lastDay!;i++ {
                var x:CGFloat       = CGFloat(lastWeekday-1)*dayWidth
                var y:CGFloat       = CGFloat(0)*dayHeight
                var frame:CGRect = CGRectMake(x,y,dayWidth,dayHeight)
                
                var dayView:DayView = DayView(frame: frame, year:beforeYear,month:beforeMonth,day:i+1,weekday:lastWeekday,dayWidth:dayWidth, dayHeight:dayHeight, gray:true)
                self.addSubview(dayView)
                
                lastWeekday++
                if lastWeekday > 7 {
                    lastWeekday = 1
                }
            }
        }

        var day:Int? = self.getLastDay(year,month:month);
        if day != nil {
            //初日の曜日を取得
            var weekday:Int = self.getWeekDay(year,month: month,day:1)
            for var i:Int = 0; i < day!;i++ {
                var week:Int    = self.getWeek(year,month: month,day:i+1)
                var x:CGFloat       = CGFloat(weekday-1)*dayWidth
                var y:CGFloat       = CGFloat(week-1)*dayHeight
                var frame:CGRect = CGRectMake(x,y,dayWidth,dayHeight)
                var dayView:DayView = DayView(frame: frame, year:year,month:month,day:i+1,weekday:weekday,dayWidth:dayWidth, dayHeight:dayHeight,gray:false)
                self.addSubview(dayView)
                weekday++
                if weekday > 7 {
                    weekday = 1
                }
            }
        }
        
        var lastDayWeekday:Int = self.getWeekDay(year,month: month,day:day!)
        var lastDayWeek:Int    = self.getWeek(year,month: month,day:day!)
        if(lastDayWeekday != 7){
            var weekday:Int = self.getWeekDay(nextYear,month: nextMonth, day:1)
            for var i:Int = 0; i < 7-lastDayWeekday;i++ {
                var x:CGFloat       = CGFloat(weekday-1)*dayWidth
                var y:CGFloat       = CGFloat(lastDayWeek-1)*dayHeight
                var frame:CGRect = CGRectMake(x,y,dayWidth,dayHeight)
                
                var dayView:DayView = DayView(frame: frame, year:nextYear,month:nextMonth,day:i+1,weekday:weekday,dayWidth:dayWidth, dayHeight:dayHeight,gray:true)
                self.addSubview(dayView)
                
                weekday++
                if weekday > 7 {
                    weekday = 1
                }
            }
        }else if(lastDayWeek == 4){
            lastDayWeekday = 0;
            var weekday:Int = self.getWeekDay(nextYear,month: nextMonth, day:1)
            for var i:Int = 0; i < 7-lastDayWeekday;i++ {
                var x:CGFloat       = CGFloat(weekday-1)*dayWidth
                var y:CGFloat       = CGFloat(lastDayWeek)*dayHeight
                var frame:CGRect = CGRectMake(x,y,dayWidth,dayHeight)
                
                var dayView:DayView = DayView(frame: frame, year:nextYear,month:nextMonth,day:i+1,weekday:weekday,dayWidth:dayWidth, dayHeight:dayHeight,gray:true)
                self.addSubview(dayView)
                
                weekday++
                if weekday > 7 {
                    weekday = 1
                }
            }
        }
    }
    
    //その月の最終日の取得
    func getLastDay(var year:Int,var month:Int) -> Int?{
        var dateFormatter:NSDateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "yyyy/MM/dd";
        if month == 12 {
            month = 0
            year++
        }
        var targetDate:NSDate? = dateFormatter.dateFromString(String(format:"%04d/%02d/01",year,month+1));
        if targetDate != nil {
            //月初から一日前を計算し、月末の日付を取得
            var orgDate = NSDate(timeInterval:(24*60*60)*(-1), sinceDate: targetDate!)
            var str:String = dateFormatter.stringFromDate(orgDate)
            //lastPathComponentを利用するのは目的として違う気も。。
            return str.lastPathComponent.toInt();
        }
        
        return nil;
    }
    
    //曜日の取得
    func getWeek(year:Int,month:Int,day:Int) ->Int{
        var dateFormatter:NSDateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "yyyy/MM/dd";
        var date:NSDate? = dateFormatter.dateFromString(String(format:"%04d/%02d/%02d",year,month,day));
        if date != nil {
            var calendar:NSCalendar = NSCalendar.currentCalendar()
            var dateComp:NSDateComponents = calendar.components(NSCalendarUnit.WeekOfMonthCalendarUnit, fromDate: date!)
            return dateComp.weekOfMonth;
        }
        return 0;
    }
    
    //第何週の取得
    func getWeekDay(year:Int,month:Int,day:Int) ->Int{
        var dateFormatter:NSDateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "yyyy/MM/dd";
        var date:NSDate? = dateFormatter.dateFromString(String(format:"%04d/%02d/%02d",year,month,day));
        if date != nil {
            var calendar:NSCalendar = NSCalendar.currentCalendar()
            var dateComp:NSDateComponents = calendar.components(NSCalendarUnit.WeekdayCalendarUnit, fromDate: date!)
            return dateComp.weekday;
        }
        return 0;
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesEnded(touches as Set<NSObject>, withEvent: event)
        for touch: AnyObject in touches {
            var t: UITouch = touch as! UITouch
            if(t.view.tag > 10000){
                if(self.selTag != 999999){
                    var label:UILabel = self.viewWithTag(self.selTag)as! UILabel
                    label.backgroundColor = UIColor.clearColor()
                }
                self.selTag = t.view.tag
            }
        }
    }
}