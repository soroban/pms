//
//  OriginalTabBarController.swift
//  pms
//
//  Created by soroban11 on 2014/12/17.
//  Copyright (c) 2014年 soroban. All rights reserved.
//

import UIKit
import GoogleMobileAds

class OriginalTabBarController: UITabBarController {
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    override func viewDidLoad() {
        super.viewDidLoad()

        if let navFont = UIFont(name: "HelveticaNeue-bold", size: 20){
            UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName : navFont,  NSForegroundColorAttributeName : UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)]
        }
        //tabbarの色とフォント設定
        let font:UIFont! = UIFont(name:"HelveticaNeue",size:10)
        let selectedAttributes:NSDictionary! = [NSFontAttributeName : font, NSForegroundColorAttributeName : UIColor(red:0.72, green:0.16, blue:0.18, alpha:1.0)]
//        UITabBarItem.appearance().setTitleTextAttributes(selectedAttributes, forState: UIControlState.Selected)
        UITabBar.appearance().tintColor = UIColor(red: 0.72, green: 0.16, blue: 0.18, alpha: 1.0)
        
        //広告設定
//        var tabHeight = self.tabBar.frame.size.height
//        var bannerView:GADBannerView = GADBannerView()
//        bannerView = GADBannerView(adSize:kGADAdSizeBanner)
//        //var yPoint =  UIScreen.mainScreen().bounds.size.height - tabHeight - bannerView.frame.height
//        var yPoint =  UIScreen.mainScreen().applicationFrame.size.height - tabHeight-bannerView.frame.height+20
//        bannerView.frame = CGRectMake(0, yPoint, UIScreen.mainScreen().bounds.size.width, bannerView.frame.height)
//        bannerView.adUnitID = "ca-app-pub-8256083710740545/8016929713"
//        bannerView.rootViewController = self
//
//        self.view.addSubview(bannerView)
//        bannerView.loadRequest(GADRequest())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
