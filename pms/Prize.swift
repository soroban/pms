//
//  Prize.swift
//  pms
//
//  Created by soroban11 on 2015/02/17.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import Foundation
import CoreData

class Prize: NSManagedObject {

    @NSManaged var createdAt: NSDate
    @NSManaged var disp: NSNumber
    @NSManaged var gift: String
    @NSManaged var id: NSNumber
    @NSManaged var imagePath: String
    @NSManaged var lastUsedAt: NSDate
    @NSManaged var num: NSNumber
    @NSManaged var order: NSNumber
    @NSManaged var point: NSNumber

}
