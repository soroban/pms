//
//  PrizeCustomCell.swift
//  pms
//
//  Created by soroban11 on 2015/02/13.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

class PrizeCustomCell: UITableViewCell {

    @IBOutlet var prizeImage: UIImageView!
    @IBOutlet var prizeLabel: UILabel!
    @IBOutlet var pointLabel: UILabel!
    @IBOutlet var unitLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    // セル内セット
    func configureCell(prize:Prize, atIndexPath indexPath: NSIndexPath){
        prizeLabel.text = prize.gift
        pointLabel.text = "\(prize.point)"
        pointLabel.textColor = UIColor(red: 0.72, green: 0.16, blue: 0.18, alpha: 1.0)
        unitLabel.textColor = UIColor(red: 0.72, green: 0.16, blue: 0.18, alpha: 1.0)
        prizeLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
        
        var path:String! = NSBundle.mainBundle().pathForResource("m_e_others_500.png", ofType: nil)
        if(!prize.imagePath.isEmpty){
            path = String(format:"%@/Documents/thumb_%@",NSHomeDirectory(), prize.imagePath)
        }
        var anError : NSError?
        var sceneData = NSData(contentsOfFile:path, options: NSDataReadingOptions.DataReadingUncached, error: &anError)
        var image : UIImage? = UIImage(data: sceneData!)
        prizeImage.image = nil
        prizeImage.image = image
    }
}
