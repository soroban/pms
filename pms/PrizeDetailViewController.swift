//
//  PrizeDetailViewController.swift
//  pms
//
//  Created by soroban11 on 2015/02/12.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit
import CoreData

class PrizeDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PrizeDetailHowmanyViewControllerDelegate,UIActionSheetDelegate, UIAlertViewDelegate{
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    //カメラ or ライブラリーから選択された画像データ
    var selectImage: UIImage!
    
    // Tableで使用する配列を定義する.
    var myItems: NSArray = ["", ""]
    
    var selectId:Int!
    var howMany:Int = 1
    
    var prizeDetail:Prize!
    
    var faCellHeight:CGFloat!
    var secCellHeight:CGFloat = 52
    var fourCellHeight:CGFloat = 61
    
    var myImage:UIImage!
    var totalPoint:Int!
    var nowPoint:Int!
    var minusPoint:Int!
    
    @IBOutlet var addButton: UIButton!
    @IBOutlet var myTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //idを元にprizeを取得
        self.prizeDetail = self.getPrizeFromId()
        var path:String! = NSBundle.mainBundle().pathForResource("l_e_others_500.png", ofType: nil)
        if(!self.prizeDetail.imagePath.isEmpty){
            path = String(format:"%@/Documents/%@",NSHomeDirectory(), self.prizeDetail.imagePath)
        }
        var anError : NSError?
        var sceneData = NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingUncached, error: &anError)
        self.myImage = UIImage(data: sceneData!)
        var scale = self.app.screenWidth/self.myImage.size.width
        self.faCellHeight = self.myImage.size.height * scale
        
        var v:UIView = UIView(frame: CGRectZero)
        self.myTable.rowHeight = UITableViewAutomaticDimension
        self.myTable.tableFooterView = v
        if self.myTable.respondsToSelector("layoutMargins") {
            self.myTable.layoutMargins = UIEdgeInsetsZero
        }
        self.myTable.separatorInset = UIEdgeInsetsZero
        self.myItems = [prizeDetail.gift, prizeDetail.point]
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("selectCount",sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    テーブルに表示する配列の総数を返す.
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    // セルの値を設定
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(indexPath.row == 1){
            var cell:SaveDetailFirstCustomCell = self.myTable.dequeueReusableCellWithIdentifier("SaveDetailFirst") as! SaveDetailFirstCustomCell
            cell.configureCell(atIndexPath : indexPath)
            if cell.respondsToSelector("layoutMargins") {
                cell.layoutMargins = UIEdgeInsetsZero;
            }
            cell.mImageView.setTranslatesAutoresizingMaskIntoConstraints(true)
            cell.mImageView.frame = CGRectMake(0, 0, self.app.screenWidth, self.faCellHeight)
            cell.mImageView.image = self.myImage
            cell.mImageView.contentMode = UIViewContentMode.ScaleAspectFit
            cell.userInteractionEnabled = false
            return cell
        }
        else if(indexPath.row == 2){
            var cell:SaveDetailSecondCustomCell = self.myTable.dequeueReusableCellWithIdentifier("SaveDetailSecond") as! SaveDetailSecondCustomCell
            cell.configureCell(atIndexPath : indexPath)
            if cell.respondsToSelector("layoutMargins") {
                cell.layoutMargins = UIEdgeInsetsZero
            }
            cell.thrLabel.text = self.prizeDetail.gift
            cell.thrLabel.setTranslatesAutoresizingMaskIntoConstraints(true)
            cell.thrLabel.frame = CGRectMake(15 , 0, self.app.screenWidth-30, 65)
            cell.thrLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
            
            cell.thrPointLabel.setTranslatesAutoresizingMaskIntoConstraints(true)
            cell.thrPointLabel.layer.position = CGPoint(x:self.app.screenWidth-90, y:90)
            cell.thrPointLabel.textColor = UIColor(red: 0.72, green: 0.16, blue: 0.18, alpha: 1.0)
            cell.thrPointLabel.text = String(format:"%@", self.prizeDetail.point)
            
            cell.thrUnitLable.setTranslatesAutoresizingMaskIntoConstraints(true)
            cell.thrUnitLable.layer.position = CGPoint(x:self.app.screenWidth-27, y:93)
            cell.thrUnitLable.textColor = UIColor(red: 0.72, green: 0.16, blue: 0.18, alpha: 1.0)
            cell.userInteractionEnabled = false
            return cell
        }
        if(indexPath.row == 3){
            var cell:SaveDetailThirdCustomCell = self.myTable.dequeueReusableCellWithIdentifier("SaveDetailThird") as! SaveDetailThirdCustomCell
            cell.configureCell(atIndexPath : indexPath)
            cell.detailTextLabel?.text=NSLocalizedString("id00017",comment: "")
            cell.textLabel?.text = String(format:NSLocalizedString("id00018",comment: "") + " %d" + NSLocalizedString("id00019",comment: ""), self.howMany);
            cell.textLabel?.textColor = UIColor(red: 0.17, green: 0.17, blue: 0.17, alpha: 1.0)
            if cell.respondsToSelector("layoutMargins") {
                cell.layoutMargins = UIEdgeInsetsZero
            }
            return cell
        }
        if(indexPath.row == 0){
            var cell:SaveDetailFourthCustomCell = self.myTable.dequeueReusableCellWithIdentifier("SaveDetailFourth") as! SaveDetailFourthCustomCell
            cell.configureCell(atIndexPath : indexPath)
            cell.fButton.setTranslatesAutoresizingMaskIntoConstraints(true)
            cell.fButton.frame = CGRectMake(0, 0, self.app.screenWidth, self.fourCellHeight)//
            if cell.respondsToSelector("layoutMargins") {
                cell.layoutMargins = UIEdgeInsetsZero
            }
            //            cell.userInteractionEnabled = false
            return cell
        }
        
        var cell:UITableViewCell! = UITableViewCell()
        return cell
    }
    
    //遷移直前で呼ばれる
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "selectCount") {
            let prizeDetailHowmanyViewController: PrizeDetailHowmanyViewController = segue.destinationViewController as! PrizeDetailHowmanyViewController
            prizeDetailHowmanyViewController.delegate = self
            prizeDetailHowmanyViewController.point = self.howMany-1
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.row == 1){
            return self.faCellHeight+1
        }
        if(indexPath.row == 2){
            return 100
        }
        if(indexPath.row == 3){
            return self.secCellHeight
        }
        if(indexPath.row == 0){
            return self.fourCellHeight
        }
        return 0
    }
    
    func howmanyDidFinished(getPoint: Int){
        self.howMany = getPoint
        self.myTable.reloadData()
    }
    
    @IBAction func buttonPushed(sender : AnyObject){
        var num = Int(self.howMany) * Int(self.prizeDetail.point)
        if objc_getClass("UIAlertController") != nil {
            // push時の処理を実装
            var alertController = UIAlertController(title: NSLocalizedString("id00020",comment: ""), message: NSLocalizedString("id00021",comment: ""), preferredStyle: .Alert)
            let addAction = UIAlertAction(title: NSLocalizedString("id00022",comment: ""), style: .Default,  handler: { action in self.subPoint(num)})
            let cancelAction = UIAlertAction(title: NSLocalizedString("id00023",comment: ""), style: .Cancel, handler: nil)
        
            alertController.addAction(addAction)
            alertController.addAction(cancelAction)
            presentViewController(alertController, animated: true, completion: nil)
        }else{
            var av = UIAlertView(title:NSLocalizedString("id00020",comment: ""), message:NSLocalizedString("id00021",comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("id00023",comment: ""), otherButtonTitles: NSLocalizedString("id00022",comment: ""))
            av.tag = 1
            av.show()
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(alertView.tag == 1){
            var num = Int(self.howMany) * Int(self.prizeDetail.point)
            if (buttonIndex == alertView.cancelButtonIndex) {
            } else {
                //OK
                self.subPoint(num)
            }
        }
    }
    
    func getPrizeFromId()->Prize{
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Prize")
        var obj : NSManagedObject? = nil
        
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.fetchLimit = 1
        //ORDER BY を指定
        fetchRequest.predicate =  NSPredicate(format: "id = %d", selectId)
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Prize]!
        var prize:Prize!
        for result in results {
            prize = result
        }
        return prize
    }
    
    func subPoint(subPoint: Int){
        var sub:Bool = true
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Point")
        var obj : NSManagedObject? = nil
        self.minusPoint = subPoint
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Point]!
        self.nowPoint = 0
        if(results.isEmpty){
            //ポイントが足らない場合
            sub = false
        }else{
            for result in results {
                if(Int(result.point) < subPoint){
                    self.nowPoint = Int(result.point)
                    sub = false
                }else{
                    self.nowPoint = Int(result.point)
                    result.point = Int(result.point) - subPoint
                    self.totalPoint = Int(result.point)
                }
            }
        }
        
        if(sub == true){
            var error: NSError?
            if !managedContext.save(&error) {
                println("Could not update \(error), \(error!.userInfo)")
            }
            
            for ( var i = 1, n = self.howMany; i <= n ; i++ ) {
                //履歴を更新
                var historyId:Int = self.getHistoryMaxId() + 1
                let managedContext: NSManagedObjectContext = app.managedObjectContext!
                let entity: NSEntityDescription! = NSEntityDescription.entityForName("History", inManagedObjectContext: managedContext)
                var newData = History(entity: entity, insertIntoManagedObjectContext: managedContext)
                newData.id = historyId
                newData.content = self.prizeDetail.gift
                newData.point = self.prizeDetail.point
                newData.type = 2
                newData.createdAt =  NSDate()
                if !managedContext.save(&error) {
                    println("Could not save \(error), \(error!.userInfo)")
                }
            }
            ////////
            
            // push時の処理を実装
            if objc_getClass("UIAlertController") != nil {
                var alertController = UIAlertController(title: NSLocalizedString("id00024",comment: ""), message: NSLocalizedString("id00025",comment: "") + "\n\n\(self.nowPoint)" + NSLocalizedString("id00026",comment: "") + "\n  - \(self.minusPoint)" + NSLocalizedString("id00027",comment: "")  + "\n\n" + NSLocalizedString("id00028",comment: "") + "\(self.totalPoint)" + NSLocalizedString("id00029",comment: ""), preferredStyle: .Alert)
                let subAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(subAction)
                presentViewController(alertController, animated: true, completion: nil)
            }else{
                var av = UIAlertView(title: NSLocalizedString("id00024",comment: ""), message: NSLocalizedString("id00025",comment: "") + "\n\n\(self.nowPoint)" + NSLocalizedString("id00026",comment: "") + "\n  - \(self.minusPoint)" + NSLocalizedString("id00027",comment: "")  + "\n\n" + NSLocalizedString("id00028",comment: "") + "\(self.totalPoint)" + NSLocalizedString("id00029",comment: ""), delegate: self, cancelButtonTitle: nil, otherButtonTitles: "OK")
                av.show()
            }
        }else{
            if objc_getClass("UIAlertController") != nil {
                var alertController = UIAlertController(title: NSLocalizedString("id00030",comment: ""), message: "\(self.nowPoint)" + NSLocalizedString("id00029",comment: "") + NSLocalizedString("id00028",comment: "") + "\n  " + NSLocalizedString("id00031",comment: "") + "\(self.minusPoint-self.nowPoint)" + NSLocalizedString("id00032",comment: ""), preferredStyle: .Alert)
                let subAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(subAction)
                presentViewController(alertController, animated: true, completion: nil)
            }else{
                var av = UIAlertView(title: NSLocalizedString("id00030",comment: ""), message:"\(self.nowPoint)" + NSLocalizedString("id00029",comment: "") + NSLocalizedString("id00028",comment: "") + "\n  " + NSLocalizedString("id00031",comment: "") + "\(self.minusPoint-self.nowPoint)" + NSLocalizedString("id00032",comment: ""), delegate: self, cancelButtonTitle: nil, otherButtonTitles: "OK")
                av.tag = 2
                av.show()
            }
        }
    }
    

    //履歴へ新規追加するためにHistoryのIDの最大値を取得
    func getHistoryMaxId() -> (Int){
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "History")
        var obj : NSManagedObject? = nil
        
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.fetchLimit = 1
        //ORDER BY を指定
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = [sortDescriptor]
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [History]!
        var returnId:Int = 0
        for result in results {
            returnId = Int(result.id)
        }
        return returnId
    }
}