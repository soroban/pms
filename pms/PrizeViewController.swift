//
//  PrizeViewController.swift
//  pms
//
//  Created by soroban11 on 2015/02/12.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit
import CoreData

class PrizeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationBarDelegate, UISearchBarDelegate,UISearchDisplayDelegate{
    var app:AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    // テーブルを用意
    @IBOutlet var myTable: UITableView!
    
    
    @IBOutlet var mySearchBar: UISearchBar!
    var myValues = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let v:UIView = UIView(frame:CGRectMake(0, 0, UIScreen.mainScreen().applicationFrame.width, 70));
        var frame:CGRect = CGRectMake(CGFloat(0),
            CGFloat(0),
            CGFloat(UIScreen.mainScreen().applicationFrame.width-75),
            CGFloat(70))
        
        var topLabel = UILabel(frame: frame)
        topLabel.textAlignment = NSTextAlignment.Center
        topLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        topLabel.textColor = UIColor(red:0.17, green:0.17, blue:0.17, alpha:1.0)
        topLabel.text = NSLocalizedString("id00000",comment: "")
        v.addSubview(topLabel)
        
        var rect:CGRect = CGRectMake((app.screenWidth/2)-90, 25, 25, 25);
        let countryCode = NSLocale.preferredLanguages().first as! String
        if(countryCode == "ja"){
            var rect:CGRect = CGRectMake((app.screenWidth/2)-50, 25, 25, 25);
        }
        var image = UIImage(named: "airplane_takeoff.png")
        var imageView = UIImageView(image: image)
        imageView.frame = rect;
        v.addSubview(imageView)
        self.navigationItem.titleView = v
    }
    
    override func viewWillAppear(animated: Bool) {
        self.myValues = []
        self.getPrizeFromSearch(self.mySearchBar.text)
        self.myTable.allowsSelectionDuringEditing = true
        // テーブル情報を更新する
        self.myTable.reloadData()
        self.pushFinishBtn()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    Cellが選択された際に呼び出される.
    */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if(self.editing){
            performSegueWithIdentifier("goToAdd",sender: nil)
        }else{
            performSegueWithIdentifier("goToDetail",sender: nil)
        }
    }
    
    //遷移直前で呼ばれる
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        var indexPath:NSIndexPath!
        if self.searchDisplayController!.active{
            indexPath = self.searchDisplayController!.searchResultsTableView.indexPathForSelectedRow()!
        }else{
            if((myTable.indexPathForSelectedRow()) != nil){
                indexPath = self.myTable.indexPathForSelectedRow()!
            }
        }
        if (segue.identifier == "goToDetail") {
            var selectPrize:Prize = self.myValues[indexPath.row] as! Prize
            let prizeDetailViewController: PrizeDetailViewController = segue.destinationViewController as! PrizeDetailViewController
            prizeDetailViewController.selectId = Int(selectPrize.id)
        }
        if (segue.identifier == "goToAdd") {
            if((myTable.indexPathForSelectedRow()) != nil){
                var selectPrize:Prize = self.myValues[indexPath.row] as! Prize
                let addPrizeNavigationController: AddPrizeNavigationController = segue.destinationViewController as! AddPrizeNavigationController
                addPrizeNavigationController.selectId = Int(selectPrize.id)
            }
        }
    }
    
    /*
    テーブルに表示する配列の総数を返す.
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myValues.count
    }
    
    // セルの値を設定
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: PrizeCustomCell = self.myTable.dequeueReusableCellWithIdentifier("prizeCustomCell") as! PrizeCustomCell
        cell.selectionStyle = UITableViewCellSelectionStyle.Blue
        cell.configureCell(self.myValues[indexPath.row] as! Prize, atIndexPath : indexPath)
        return cell
    }
    
    //[編集]がクリックされたら編集モードへ
    func pushEditBtn() {
        for subView in self.mySearchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.enabled = false
                }
            }
        }

        //テーブルをeditmodeへ変更
        self.setEditing(true, animated: true)
        var finish:UIBarButtonItem  = UIBarButtonItem(title: "Finish", style: .Plain, target: self, action: "pushFinishBtn")
        finish.title = NSLocalizedString("id00001",comment: "")
        self.navigationItem.leftBarButtonItem = finish;
    }
    
    //[完了]がクリックされたら編集モードを終了
    func pushFinishBtn() {
        //テーブルのeditmodeを終了
        for subView in self.mySearchBar.subviews  {
            for subsubView in subView.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.enabled = true
                }
            }
        }

        self.setEditing(false, animated: true)
        var finish:UIBarButtonItem  = UIBarButtonItem(title: "Edit", style: .Plain, target: self, action: "pushEditBtn")
        finish.title = NSLocalizedString("id00002",comment: "")
        self.navigationItem.leftBarButtonItem = finish;
        
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.myTable.setEditing(editing, animated: animated)
    }
    
    func getPrize() {
        self.myValues = []
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Prize")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        //ORDER BY を指定
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = [sortDescriptor]
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Prize]!
        for result in results {
            self.myValues.addObject(result)
        }
    }
    
    func getPrizeFromSearch(keyword:String) {
        self.myValues = []
        let managedContext: NSManagedObjectContext = app.managedObjectContext!
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Prize")
        var obj : NSManagedObject? = nil
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate =  NSPredicate(format: "gift LIKE[cd] %@", "*" + keyword + "*")
        //ORDER BY を指定
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = [sortDescriptor]
        var results = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [Prize]!
        for result in results {
            self.myValues.addObject(result)
        }
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch editingStyle {
        case .Delete:
            var tempValue = self.myValues[indexPath.row] as! Prize
            //ファイルを削除
            if(!tempValue.imagePath.isEmpty){
                var path = String(format:"%@/Documents/%@",NSHomeDirectory(), tempValue.imagePath)
                let fileMan = NSFileManager.defaultManager()
                fileMan.removeItemAtPath(path, error: nil)
            }
            let managedContext: NSManagedObjectContext = app.managedObjectContext!
            managedContext.deleteObject(self.myValues[indexPath.row] as! NSManagedObject)
            managedContext.save(nil)
            
            self.myValues.removeObjectAtIndex(indexPath.row)
            self.myTable.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        default:
            return
        }
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.getPrizeFromSearch(self.mySearchBar.text)
        self.myTable.reloadData()
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.getPrize()
        self.myTable.reloadData()
        self.mySearchBar.text = ""
        self.mySearchBar.resignFirstResponder()
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.getPrizeFromSearch(self.mySearchBar.text)
        self.mySearchBar.resignFirstResponder()
        self.myTable.reloadData()
    }
    func searchDisplayController(controller: UISearchDisplayController, didLoadSearchResultsTableView tableView: UITableView) {
        tableView.rowHeight = self.myTable.rowHeight
    }

    
//    // MARK: UITableView DataSource
//    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//        return true
//    }
//    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//        return true
//    }
//    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
//        // sourceIndexPath: 元の場所のIndexPath
//        // destinationIndexPath: 新しい場所のIndexPath
//        let item: AnyObject = self.myValues[sourceIndexPath.row]
//        self.myValues.removeObjectAtIndex(sourceIndexPath.row)
//        self.myValues.insertObject(item, atIndex: destinationIndexPath.row)
//    }
}