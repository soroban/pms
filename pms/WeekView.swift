//
//  WeekView.swift
//  pms
//
//  Created by 大口 尚紀(管理者) on 2015/02/25.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import UIKit

class WeekView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    init(frame:CGRect,weekName:String, weekDay:Int){
        super.init(frame: frame)
        var dayWidth:Int = Int( (UIScreen.mainScreen().bounds.size.width) / 7.0 )
        var dayHeight:CGFloat = 30
        var dayLabel:UILabel = UILabel(frame: CGRectMake(0, 0, CGFloat(dayWidth),dayHeight))
        dayLabel.textAlignment = NSTextAlignment.Center
        if weekDay == 0 {
            //日曜日は赤
            dayLabel.textColor = UIColor.redColor()
        } else if weekDay == 6 {
            //土曜日は青
            dayLabel.textColor = UIColor.blueColor()
        }

        dayLabel.text = String(format:"%@", weekName)
        self.addSubview(dayLabel)
    }
}