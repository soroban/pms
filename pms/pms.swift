//
//  pms.swift
//  pms
//
//  Created by 大口 尚紀(管理者) on 2015/01/15.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import Foundation
import CoreData

class pms: NSManagedObject {

    @NSManaged var createdAt: NSDate
    @NSManaged var gift: String
    @NSManaged var id: NSNumber
    @NSManaged var imagePath: String
    @NSManaged var lastUsedAt: NSDate
    @NSManaged var num: NSNumber
    @NSManaged var point: NSNumber
    @NSManaged var disp: NSNumber

}
