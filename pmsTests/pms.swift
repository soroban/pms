//
//  pms.swift
//  pms
//
//  Created by 大口 尚紀(管理者) on 2015/04/09.
//  Copyright (c) 2015年 soroban. All rights reserved.
//

import Foundation
import CoreData

class pms: NSManagedObject {

    @NSManaged var aim: String
    @NSManaged var createdAt: NSDate
    @NSManaged var distp_flg: NSNumber

}
